<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_checklist_pengelola extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('checklist_pengelola', $data);
    }
    public function read()
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = checklist_pengelola.id_pengelola_teknisi');
        $this->db->join('atm', 'atm.id_atm = checklist_pengelola.id_atm');
        $this->db->order_by('created_checklist_pengelola', 'DESC');
        return $this->db->get('checklist_pengelola');
    }
    public function read_where($array)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = checklist_pengelola.id_pengelola_teknisi');
        $this->db->join('atm', 'atm.id_atm = checklist_pengelola.id_atm');
        $this->db->order_by('created_checklist_pengelola', 'DESC');
        return $this->db->get_where('checklist_pengelola', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = checklist_pengelola.id_pengelola_teknisi');
        $this->db->join('atm', 'atm.id_atm = checklist_pengelola.id_atm');
        $this->db->order_by('created_checklist_pengelola', 'DESC');
        return $this->db->get('checklist_pengelola', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = checklist_pengelola.id_pengelola_teknisi');
        $this->db->join('atm', 'atm.id_atm = checklist_pengelola.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_pengelola', 'DESC');
        return $this->db->get('checklist_pengelola');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = checklist_pengelola.id_pengelola_teknisi');
        $this->db->join('atm', 'atm.id_atm = checklist_pengelola.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_pengelola', 'DESC');
        return $this->db->get('checklist_pengelola', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('checklist_pengelola', $data, ['id_checklist_pengelola' => $id]);
    }
    public function delete($id)
    {
        $tables = array('checklist_pengelola');
        $this->db->where('id_checklist_pengelola', $id);
        $this->db->delete($tables);
    }
}
