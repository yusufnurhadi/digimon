<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengelola_teknisi extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('pengelola_teknisi', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = pengelola_teknisi.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola_teknisi.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola_teknisi');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = pengelola_teknisi.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola_teknisi.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get_where('pengelola_teknisi', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = pengelola_teknisi.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola_teknisi.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola_teknisi', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = pengelola_teknisi.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola_teknisi.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola_teknisi');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = pengelola_teknisi.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola_teknisi.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola_teknisi', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('pengelola_teknisi', $data, ['id_pengelola_teknisi' => $id]);
    }
    public function delete($username)
    {
        $tables = array('pengelola_teknisi', 'user');
        $this->db->where('email_user', $username);
        $this->db->delete($tables);
    }
}
