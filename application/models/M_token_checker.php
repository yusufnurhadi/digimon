<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_checker extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_checker', $data);
    }
    public function read()
    {
        $this->db->join('checker', 'checker.id_checker = token_checker.id_checker');
        $this->db->order_by('created_token_checker', 'DESC');
        return $this->db->get('token_checker');
    }
    public function read_where($array)
    {
        $this->db->join('checker', 'checker.id_checker = token_checker.id_checker');
        $this->db->order_by('created_token_checker', 'DESC');
        return $this->db->get_where('token_checker', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('checker', 'checker.id_checker = token_checker.id_checker');
        $this->db->order_by('created_token_checker', 'DESC');
        return $this->db->get('token_checker', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('checker', 'checker.id_checker = token_checker.id_checker');
        $this->db->like($array);
        $this->db->order_by('created_token_checker', 'DESC');
        return $this->db->get('token_checker');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('checker', 'checker.id_checker = token_checker.id_checker');
        $this->db->like($array);
        $this->db->order_by('created_token_checker', 'DESC');
        return $this->db->get('token_checker', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_checker', $data, ['id_token_checker' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_checker');
        $this->db->where('id_token_checker', $id);
        $this->db->delete($tables);
    }
}
