<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_admin extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('admin', $data);
	}
	public function read()
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('admin');
	}
	public function read_where($array)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get_where('admin', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('admin', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->like($array);
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('admin');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->like($array);
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('admin', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('admin', $data, ['id_admin' => $id]);
	}
	public function delete($username)
	{
		$tables = array('admin', 'user');
		$this->db->where('email_user', $username);
		$this->db->delete($tables);
	}
}
