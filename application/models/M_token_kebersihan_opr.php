<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_kebersihan_opr extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_kebersihan_opr', $data);
    }
    public function read()
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = token_kebersihan_opr.id_kebersihan_opr');
        $this->db->order_by('created_token_kebersihan_opr', 'DESC');
        return $this->db->get('token_kebersihan_opr');
    }
    public function read_where($array)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = token_kebersihan_opr.id_kebersihan_opr');
        $this->db->order_by('created_token_kebersihan_opr', 'DESC');
        return $this->db->get_where('token_kebersihan_opr', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = token_kebersihan_opr.id_kebersihan_opr');
        $this->db->order_by('created_token_kebersihan_opr', 'DESC');
        return $this->db->get('token_kebersihan_opr', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = token_kebersihan_opr.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_token_kebersihan_opr', 'DESC');
        return $this->db->get('token_kebersihan_opr');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = token_kebersihan_opr.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_token_kebersihan_opr', 'DESC');
        return $this->db->get('token_kebersihan_opr', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_kebersihan_opr', $data, ['id_token_kebersihan_opr' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_kebersihan_opr');
        $this->db->where('id_token_kebersihan_opr', $id);
        $this->db->delete($tables);
    }
}
