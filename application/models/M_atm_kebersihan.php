<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_kebersihan extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_kebersihan', $data);
    }
    public function read()
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_kebersihan.kd_atm_cabang');
        $this->db->order_by('created_atm_kebersihan', 'DESC');
        return $this->db->get('atm_kebersihan');
    }
    public function read_where($array)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_kebersihan.kd_atm_cabang');
        $this->db->order_by('created_atm_kebersihan', 'DESC');
        return $this->db->get_where('atm_kebersihan', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_kebersihan.kd_atm_cabang');
        $this->db->order_by('created_atm_kebersihan', 'DESC');
        return $this->db->get('atm_kebersihan', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_kebersihan.kd_atm_cabang');
        $this->db->like($array);
        $this->db->order_by('created_atm_kebersihan', 'DESC');
        return $this->db->get('atm_kebersihan');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_kebersihan.kd_atm_cabang');
        $this->db->like($array);
        $this->db->order_by('created_atm_kebersihan', 'DESC');
        return $this->db->get('atm_kebersihan', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_kebersihan', $data, ['id_atm_kebersihan' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_kebersihan');
        $this->db->where('id_atm_kebersihan', $id);
        $this->db->delete($tables);
    }
}
