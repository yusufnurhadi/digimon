<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_usage extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('atm_usage', $data);
	}
	public function read()
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->order_by('created_atm_usage', 'DESC');
		return $this->db->get('atm_usage');
	}
	public function read_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->order_by('created_atm_usage', 'DESC');
		return $this->db->get_where('atm_usage', $array);
	}
	public function read_where_usage($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->order_by('month_atm_usage', 'ASC');
		$this->db->order_by('year_atm_usage', 'ASC');
		return $this->db->get_where('atm_usage', $array);
	}
	public function read_where_group($array, $group)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->group_by($group);
		return $this->db->get_where('atm_usage', $array);
	}
	public function sum_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->select_sum('value_atm_usage');
		return $this->db->get_where('atm_usage', $array);
	}
	public function avg_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->select_avg('value_atm_usage');
		return $this->db->get_where('atm_usage', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->order_by('created_atm_usage', 'DESC');
		return $this->db->get('atm_usage', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_atm_usage', 'DESC');
		return $this->db->get('atm_usage');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = atm_usage.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_atm_usage', 'DESC');
		return $this->db->get('atm_usage', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('atm_usage', $data, ['id_atm_usage' => $id]);
	}
	public function delete($id)
	{
		$tables = array('atm_usage');
		$this->db->where('id_atm_usage', $id);
		$this->db->delete($tables);
	}
}