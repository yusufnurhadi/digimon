<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_fbi extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('atm_fbi', $data);
	}
	public function read()
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->order_by('created_atm_fbi', 'DESC');
		return $this->db->get('atm_fbi');
	}
	public function read_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->order_by('created_atm_fbi', 'DESC');
		return $this->db->get_where('atm_fbi', $array);
	}
	public function read_where_fbi($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->order_by('month_atm_fbi', 'ASC');
		$this->db->order_by('year_atm_fbi', 'ASC');
		return $this->db->get_where('atm_fbi', $array);
	}
	public function read_where_group($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->group_by('atm_fbi.id_atm');
		return $this->db->get_where('atm_fbi', $array);
	}
	public function sum_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->select_sum('value_atm_fbi');
		return $this->db->get_where('atm_fbi', $array);
	}
	public function avg_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->select_avg('value_atm_fbi');
		return $this->db->get_where('atm_fbi', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->order_by('created_atm_fbi', 'DESC');
		return $this->db->get('atm_fbi', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_atm_fbi', 'DESC');
		return $this->db->get('atm_fbi');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = atm_fbi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_atm_fbi', 'DESC');
		return $this->db->get('atm_fbi', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('atm_fbi', $data, ['id_atm_fbi' => $id]);
	}
	public function delete($id)
	{
		$tables = array('atm_fbi');
		$this->db->where('id_atm_fbi', $id);
		$this->db->delete($tables);
	}
}