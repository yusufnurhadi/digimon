<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kebersihan_opr extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('kebersihan_opr', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = kebersihan_opr.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan_opr.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan_opr');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = kebersihan_opr.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan_opr.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get_where('kebersihan_opr', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = kebersihan_opr.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan_opr.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan_opr', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = kebersihan_opr.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan_opr.id_atm_kebersihan');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan_opr');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = kebersihan_opr.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan_opr.id_atm_kebersihan');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan_opr', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('kebersihan_opr', $data, ['id_kebersihan_opr' => $id]);
    }
    public function delete($username)
    {
        $tables = array('kebersihan_opr', 'user');
        $this->db->where('email_user', $username);
        $this->db->delete($tables);
    }
}
