<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_sla extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_sla', $data);
    }
    public function read()
    {
        $this->db->join('atm', 'atm.id_atm = atm_sla.id_atm');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_sla.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm_sla.kd_atm_pengelola');
        $this->db->order_by('created_atm_sla', 'DESC');
        return $this->db->get('atm_sla');
    }
    public function read_where($array)
    {
        $this->db->join('atm', 'atm.id_atm = atm_sla.id_atm');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_sla.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm_sla.kd_atm_pengelola');
        $this->db->order_by('created_atm_sla', 'DESC');
        return $this->db->get_where('atm_sla', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm', 'atm.id_atm = atm_sla.id_atm');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_sla.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm_sla.kd_atm_pengelola');
        $this->db->order_by('created_atm_sla', 'DESC');
        return $this->db->get('atm_sla', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm', 'atm.id_atm = atm_sla.id_atm');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_sla.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm_sla.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_atm_sla', 'DESC');
        return $this->db->get('atm_sla');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm', 'atm.id_atm = atm_sla.id_atm');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm_sla.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm_sla.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_atm_sla', 'DESC');
        return $this->db->get('atm_sla', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_sla', $data, ['id_atm_sla' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_sla');
        $this->db->where('id_atm_sla', $id);
        $this->db->delete($tables);
    }
}
