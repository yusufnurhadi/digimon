<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_problem_finish extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_problem_finish', $data);
    }
    public function read()
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_finish.id_atm_problem');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_finish.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_finish', 'DESC');
        return $this->db->get('atm_problem_finish');
    }
    public function read_where($array)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_finish.id_atm_problem');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_finish.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_finish', 'DESC');
        return $this->db->get_where('atm_problem_finish', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_finish.id_atm_problem');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_finish.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_finish', 'DESC');
        return $this->db->get('atm_problem_finish', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_finish.id_atm_problem');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_finish.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem_finish', 'DESC');
        return $this->db->get('atm_problem_finish');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_finish.id_atm_problem');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_finish.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem_finish', 'DESC');
        return $this->db->get('atm_problem_finish', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_problem_finish', $data, ['id_atm_problem_finish' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_problem_finish');
        $this->db->where('id_atm_problem_finish', $id);
        $this->db->delete($tables);
    }
}
