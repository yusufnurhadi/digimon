<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_cabang extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('atm_cabang', $data);
	}
	public function read()
	{
		$this->db->order_by('created_atm_cabang', 'DESC');
		return $this->db->get('atm_cabang');
	}
	public function read_where($array)
	{
		$this->db->order_by('created_atm_cabang', 'DESC');
		return $this->db->get_where('atm_cabang', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->order_by('created_atm_cabang', 'DESC');
		return $this->db->get('atm_cabang', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->like($array);
		$this->db->order_by('created_atm_cabang', 'DESC');
		return $this->db->get('atm_cabang');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->like($array);
		$this->db->order_by('created_atm_cabang', 'DESC');
		return $this->db->get('atm_cabang', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('atm_cabang', $data, ['kd_atm_cabang' => $id]);
	}
	public function delete($id)
	{
		$tables = array('atm_cabang');
		$this->db->where('kd_atm_cabang', $id);
		$this->db->delete($tables);
	}
}
