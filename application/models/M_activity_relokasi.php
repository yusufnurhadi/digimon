<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_activity_relokasi extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('activity_relokasi', $data);
	}
	public function read()
	{
		$this->db->join('atm', 'atm.id_atm = activity_relokasi.id_atm');
		$this->db->order_by('created_activity_relokasi', 'DESC');
		return $this->db->get('activity_relokasi');
	}
	public function read_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = activity_relokasi.id_atm');
		$this->db->order_by('created_activity_relokasi', 'DESC');
		return $this->db->get_where('activity_relokasi', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = activity_relokasi.id_atm');
		$this->db->order_by('created_activity_relokasi', 'DESC');
		return $this->db->get('activity_relokasi', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('atm', 'atm.id_atm = activity_relokasi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_activity_relokasi', 'DESC');
		return $this->db->get('activity_relokasi');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = activity_relokasi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_activity_relokasi', 'DESC');
		return $this->db->get('activity_relokasi', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('activity_relokasi', $data, ['id_activity_relokasi' => $id]);
	}
	public function delete($id)
	{
		$tables = array('activity_relokasi');
		$this->db->where('id_activity_relokasi', $id);
		$this->db->delete($tables);
	}
}
