<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kebersihan extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('kebersihan', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = kebersihan.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = kebersihan.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get_where('kebersihan', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = kebersihan.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan.id_atm_kebersihan');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = kebersihan.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan.id_atm_kebersihan');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = kebersihan.email_user');
        $this->db->join('atm_kebersihan', 'atm_kebersihan.id_atm_kebersihan = kebersihan.id_atm_kebersihan');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('kebersihan', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('kebersihan', $data, ['id_kebersihan' => $id]);
    }
    public function delete($username)
    {
        $tables = array('kebersihan', 'user');
        $this->db->where('email_user', $username);
        $this->db->delete($tables);
    }
}
