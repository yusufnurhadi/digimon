<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_checker extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('checker', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = checker.email_user');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = checker.kd_atm_cabang');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('checker');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = checker.email_user');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = checker.kd_atm_cabang');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get_where('checker', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = checker.email_user');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = checker.kd_atm_cabang');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('checker', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = checker.email_user');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = checker.kd_atm_cabang');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('checker');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = checker.email_user');
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = checker.kd_atm_cabang');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('checker', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('checker', $data, ['id_checker' => $id]);
    }
    public function delete($username)
    {
        $tables = array('checker', 'user');
        $this->db->where('email_user', $username);
        $this->db->delete($tables);
    }
}
