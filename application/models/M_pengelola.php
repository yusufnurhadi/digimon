<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengelola extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('pengelola', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = pengelola.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = pengelola.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get_where('pengelola', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = pengelola.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola.kd_atm_pengelola');
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = pengelola.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = pengelola.email_user');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = pengelola.kd_atm_pengelola');
        $this->db->like($array);
        $this->db->order_by('created_user', 'DESC');
        return $this->db->get('pengelola', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('pengelola', $data, ['id_pengelola' => $id]);
    }
    public function delete($username)
    {
        $tables = array('pengelola', 'user');
        $this->db->where('email_user', $username);
        $this->db->delete($tables);
    }
}
