<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_checklist_kebersihan extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('checklist_kebersihan', $data);
    }
    public function read()
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = checklist_kebersihan.id_kebersihan_opr');
        $this->db->join('atm', 'atm.id_atm = checklist_kebersihan.id_atm');
        $this->db->order_by('created_checklist_kebersihan', 'DESC');
        return $this->db->get('checklist_kebersihan');
    }
    public function read_where($array)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = checklist_kebersihan.id_kebersihan_opr');
        $this->db->join('atm', 'atm.id_atm = checklist_kebersihan.id_atm');
        $this->db->order_by('created_checklist_kebersihan', 'DESC');
        return $this->db->get_where('checklist_kebersihan', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = checklist_kebersihan.id_kebersihan_opr');
        $this->db->join('atm', 'atm.id_atm = checklist_kebersihan.id_atm');
        $this->db->order_by('created_checklist_kebersihan', 'DESC');
        return $this->db->get('checklist_kebersihan', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = checklist_kebersihan.id_kebersihan_opr');
        $this->db->join('atm', 'atm.id_atm = checklist_kebersihan.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_kebersihan', 'DESC');
        return $this->db->get('checklist_kebersihan');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = checklist_kebersihan.id_kebersihan_opr');
        $this->db->join('atm', 'atm.id_atm = checklist_kebersihan.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_kebersihan', 'DESC');
        return $this->db->get('checklist_kebersihan', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('checklist_kebersihan', $data, ['id_checklist_kebersihan' => $id]);
    }
    public function delete($id)
    {
        $tables = array('checklist_kebersihan');
        $this->db->where('id_checklist_kebersihan', $id);
        $this->db->delete($tables);
    }
}
