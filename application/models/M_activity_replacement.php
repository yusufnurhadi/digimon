<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_activity_replacement extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('activity_replacement', $data);
    }
    public function read()
    {
        $this->db->order_by('created_activity_replacement', 'DESC');
        return $this->db->get('activity_replacement');
    }
    public function read_where($array)
    {
        $this->db->order_by('created_activity_replacement', 'DESC');
        return $this->db->get_where('activity_replacement', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->order_by('created_activity_replacement', 'DESC');
        return $this->db->get('activity_replacement', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        $this->db->order_by('created_activity_replacement', 'DESC');
        return $this->db->get('activity_replacement');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        $this->db->order_by('created_activity_replacement', 'DESC');
        return $this->db->get('activity_replacement', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('activity_replacement', $data, ['id_activity_replacement' => $id]);
    }
    public function delete($id)
    {
        $tables = array('activity_replacement');
        $this->db->where('id_activity_replacement', $id);
        $this->db->delete($tables);
    }
}
