<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_pengelola_teknisi extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_pengelola_teknisi', $data);
    }
    public function read()
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = token_pengelola_teknisi.id_pengelola_teknisi');
        $this->db->order_by('created_token_pengelola_teknisi', 'DESC');
        return $this->db->get('token_pengelola_teknisi');
    }
    public function read_where($array)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = token_pengelola_teknisi.id_pengelola_teknisi');
        $this->db->order_by('created_token_pengelola_teknisi', 'DESC');
        return $this->db->get_where('token_pengelola_teknisi', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = token_pengelola_teknisi.id_pengelola_teknisi');
        $this->db->order_by('created_token_pengelola_teknisi', 'DESC');
        return $this->db->get('token_pengelola_teknisi', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = token_pengelola_teknisi.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_token_pengelola_teknisi', 'DESC');
        return $this->db->get('token_pengelola_teknisi');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = token_pengelola_teknisi.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_token_pengelola_teknisi', 'DESC');
        return $this->db->get('token_pengelola_teknisi', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_pengelola_teknisi', $data, ['id_token_pengelola_teknisi' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_pengelola_teknisi');
        $this->db->where('id_token_pengelola_teknisi', $id);
        $this->db->delete($tables);
    }
}
