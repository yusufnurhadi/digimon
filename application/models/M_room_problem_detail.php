<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_room_problem_detail extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('room_problem_detail', $data);
    }
    public function read()
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_detail.id_room_problem');
        $this->db->join('kebersihan', 'kebersihan.id_kebersihan = room_problem_detail.id_kebersihan');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_detail.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_detail', 'DESC');
        return $this->db->get('room_problem_detail');
    }
    public function read_where($array)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_detail.id_room_problem');
        $this->db->join('kebersihan', 'kebersihan.id_kebersihan = room_problem_detail.id_kebersihan');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_detail.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_detail', 'DESC');
        return $this->db->get_where('room_problem_detail', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_detail.id_room_problem');
        $this->db->join('kebersihan', 'kebersihan.id_kebersihan = room_problem_detail.id_kebersihan');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_detail.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_detail', 'DESC');
        return $this->db->get('room_problem_detail', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_detail.id_room_problem');
        $this->db->join('kebersihan', 'kebersihan.id_kebersihan = room_problem_detail.id_kebersihan');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_detail.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_room_problem_detail', 'DESC');
        return $this->db->get('room_problem_detail');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_detail.id_room_problem');
        $this->db->join('kebersihan', 'kebersihan.id_kebersihan = room_problem_detail.id_kebersihan');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_detail.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_room_problem_detail', 'DESC');
        return $this->db->get('room_problem_detail', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('room_problem_detail', $data, ['id_room_problem_detail' => $id]);
    }
    public function delete($id)
    {
        $tables = array('room_problem_detail');
        $this->db->where('id_room_problem_detail', $id);
        $this->db->delete($tables);
    }
}
