<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_checklist_checker extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('checklist_checker', $data);
    }
    public function read()
    {
        $this->db->join('checker', 'checker.id_checker = checklist_checker.id_checker');
        $this->db->join('atm', 'atm.id_atm = checklist_checker.id_atm');
        $this->db->order_by('created_checklist_checker', 'DESC');
        return $this->db->get('checklist_checker');
    }
    public function read_where($array)
    {
        $this->db->join('checker', 'checker.id_checker = checklist_checker.id_checker');
        $this->db->join('atm', 'atm.id_atm = checklist_checker.id_atm');
        $this->db->order_by('created_checklist_checker', 'DESC');
        return $this->db->get_where('checklist_checker', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('checker', 'checker.id_checker = checklist_checker.id_checker');
        $this->db->join('atm', 'atm.id_atm = checklist_checker.id_atm');
        $this->db->order_by('created_checklist_checker', 'DESC');
        return $this->db->get('checklist_checker', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('checker', 'checker.id_checker = checklist_checker.id_checker');
        $this->db->join('atm', 'atm.id_atm = checklist_checker.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_checker', 'DESC');
        return $this->db->get('checklist_checker');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('checker', 'checker.id_checker = checklist_checker.id_checker');
        $this->db->join('atm', 'atm.id_atm = checklist_checker.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_checklist_checker', 'DESC');
        return $this->db->get('checklist_checker', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('checklist_checker', $data, ['id_checklist_checker' => $id]);
    }
    public function delete($id)
    {
        $tables = array('checklist_checker');
        $this->db->where('id_checklist_checker', $id);
        $this->db->delete($tables);
    }
}
