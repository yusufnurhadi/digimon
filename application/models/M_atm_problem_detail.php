<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_problem_detail extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_problem_detail', $data);
    }
    public function read()
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_detail.id_atm_problem');
        $this->db->join('pengelola', 'pengelola.id_pengelola = atm_problem_detail.id_pengelola');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_detail.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_detail', 'DESC');
        return $this->db->get('atm_problem_detail');
    }
    public function read_where($array)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_detail.id_atm_problem');
        $this->db->join('pengelola', 'pengelola.id_pengelola = atm_problem_detail.id_pengelola');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_detail.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_detail', 'DESC');
        return $this->db->get_where('atm_problem_detail', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_detail.id_atm_problem');
        $this->db->join('pengelola', 'pengelola.id_pengelola = atm_problem_detail.id_pengelola');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_detail.id_pengelola_teknisi');
        $this->db->order_by('created_atm_problem_detail', 'DESC');
        return $this->db->get('atm_problem_detail', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_detail.id_atm_problem');
        $this->db->join('pengelola', 'pengelola.id_pengelola = atm_problem_detail.id_pengelola');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_detail.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem_detail', 'DESC');
        return $this->db->get('atm_problem_detail');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm_problem', 'atm_problem.id_atm_problem = atm_problem_detail.id_atm_problem');
        $this->db->join('pengelola', 'pengelola.id_pengelola = atm_problem_detail.id_pengelola');
        $this->db->join('pengelola_teknisi', 'pengelola_teknisi.id_pengelola_teknisi = atm_problem_detail.id_pengelola_teknisi');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem_detail', 'DESC');
        return $this->db->get('atm_problem_detail', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_problem_detail', $data, ['id_atm_problem_detail' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_problem_detail');
        $this->db->where('id_atm_problem_detail', $id);
        $this->db->delete($tables);
    }
}
