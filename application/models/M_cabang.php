<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_cabang extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('cabang', $data);
	}
	public function read()
	{
		$this->db->join('user', 'user.email_user = cabang.email_user');
		$this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = cabang.kd_atm_cabang');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('cabang');
	}
	public function read_where($array)
	{
		$this->db->join('user', 'user.email_user = cabang.email_user');
		$this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = cabang.kd_atm_cabang');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get_where('cabang', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('user', 'user.email_user = cabang.email_user');
		$this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = cabang.kd_atm_cabang');
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('cabang', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('user', 'user.email_user = cabang.email_user');
		$this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = cabang.kd_atm_cabang');
		$this->db->like($array);
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('cabang');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('user', 'user.email_user = cabang.email_user');
		$this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = cabang.kd_atm_cabang');
		$this->db->like($array);
		$this->db->order_by('created_user', 'DESC');
		return $this->db->get('cabang', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('cabang', $data, ['id_cabang' => $id]);
	}
	public function delete($username)
	{
		$tables = array('cabang', 'user');
		$this->db->where('email_user', $username);
		$this->db->delete($tables);
	}
}