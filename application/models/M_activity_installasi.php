<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_activity_installasi extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('activity_installasi', $data);
	}
	public function read()
	{
		$this->db->join('atm', 'atm.id_atm = activity_installasi.id_atm');
		$this->db->order_by('created_activity_installasi', 'DESC');
		return $this->db->get('activity_installasi');
	}
	public function read_where($array)
	{
		$this->db->join('atm', 'atm.id_atm = activity_installasi.id_atm');
		$this->db->order_by('created_activity_installasi', 'DESC');
		return $this->db->get_where('activity_installasi', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = activity_installasi.id_atm');
		$this->db->order_by('created_activity_installasi', 'DESC');
		return $this->db->get('activity_installasi', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('atm', 'atm.id_atm = activity_installasi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_activity_installasi', 'DESC');
		return $this->db->get('activity_installasi');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('atm', 'atm.id_atm = activity_installasi.id_atm');
		$this->db->like($array);
		$this->db->order_by('created_activity_installasi', 'DESC');
		return $this->db->get('activity_installasi', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('activity_installasi', $data, ['id_activity_installasi' => $id]);
	}
	public function delete($id)
	{
		$tables = array('activity_installasi');
		$this->db->where('id_activity_installasi', $id);
		$this->db->delete($tables);
	}
}