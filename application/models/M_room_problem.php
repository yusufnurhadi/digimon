<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_room_problem extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('room_problem', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = room_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = room_problem.id_atm');
        $this->db->order_by('created_room_problem', 'DESC');
        return $this->db->get('room_problem');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = room_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = room_problem.id_atm');
        $this->db->order_by('created_room_problem', 'DESC');
        return $this->db->get_where('room_problem', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = room_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = room_problem.id_atm');
        $this->db->order_by('created_room_problem', 'DESC');
        return $this->db->get('room_problem', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = room_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = room_problem.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_room_problem', 'DESC');
        return $this->db->get('room_problem');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = room_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = room_problem.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_room_problem', 'DESC');
        return $this->db->get('room_problem', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('room_problem', $data, ['id_room_problem' => $id]);
    }
    public function delete($id)
    {
        $tables = array('room_problem');
        $this->db->where('id_room_problem', $id);
        $this->db->delete($tables);
    }
}
