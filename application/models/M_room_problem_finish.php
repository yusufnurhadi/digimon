<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_room_problem_finish extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('room_problem_finish', $data);
    }
    public function read()
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_finish.id_room_problem');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_finish.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_finish', 'DESC');
        return $this->db->get('room_problem_finish');
    }
    public function read_where($array)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_finish.id_room_problem');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_finish.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_finish', 'DESC');
        return $this->db->get_where('room_problem_finish', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_finish.id_room_problem');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_finish.id_kebersihan_opr');
        $this->db->order_by('created_room_problem_finish', 'DESC');
        return $this->db->get('room_problem_finish', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_finish.id_room_problem');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_finish.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_room_problem_finish', 'DESC');
        return $this->db->get('room_problem_finish');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('room_problem', 'room_problem.id_room_problem = room_problem_finish.id_room_problem');
        $this->db->join('kebersihan_opr', 'kebersihan_opr.id_kebersihan_opr = room_problem_finish.id_kebersihan_opr');
        $this->db->like($array);
        $this->db->order_by('created_room_problem_finish', 'DESC');
        return $this->db->get('room_problem_finish', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('room_problem_finish', $data, ['id_room_problem_finish' => $id]);
    }
    public function delete($id)
    {
        $tables = array('room_problem_finish');
        $this->db->where('id_room_problem_finish', $id);
        $this->db->delete($tables);
    }
}
