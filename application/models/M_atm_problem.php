<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_problem extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_problem', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = atm_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = atm_problem.id_atm');
        $this->db->order_by('created_atm_problem', 'DESC');
        return $this->db->get('atm_problem');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = atm_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = atm_problem.id_atm');
        $this->db->order_by('created_atm_problem', 'DESC');
        return $this->db->get_where('atm_problem', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = atm_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = atm_problem.id_atm');
        $this->db->order_by('created_atm_problem', 'DESC');
        return $this->db->get('atm_problem', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = atm_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = atm_problem.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem', 'DESC');
        return $this->db->get('atm_problem');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = atm_problem.email_user');
        $this->db->join('atm', 'atm.id_atm = atm_problem.id_atm');
        $this->db->like($array);
        $this->db->order_by('created_atm_problem', 'DESC');
        return $this->db->get('atm_problem', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_problem', $data, ['id_atm_problem' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_problem');
        $this->db->where('id_atm_problem', $id);
        $this->db->delete($tables);
    }
}
