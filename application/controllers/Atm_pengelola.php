<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Atm_pengelola extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('atm_pengelola/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword') && $this->input->post('change')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_atm_pengelola', $keyword);
            $this->session->set_userdata('change_atm_pengelola', $change);

            $config['total_rows'] = $this->m_atm_pengelola->read_like([
                $this->session->userdata('change_atm_pengelola') => $this->session->userdata('key_atm_pengelola'),
            ])->num_rows();
            $data['atm_pengelola'] = $this->m_atm_pengelola->read_like_pagination([
                $this->session->userdata('change_atm_pengelola') => $this->session->userdata('key_atm_pengelola'),
            ], $config['per_page'], $data['start'])->result_array();

        } else {

            if ($this->session->userdata('key_atm_pengelola')) {

                $config['total_rows'] = $this->m_atm_pengelola->read_like([
                    $this->session->userdata('change_atm_pengelola') => $this->session->userdata('key_atm_pengelola'),
                ])->num_rows();
                $data['atm_pengelola'] = $this->m_atm_pengelola->read_like_pagination([
                    $this->session->userdata('change_atm_pengelola') => $this->session->userdata('key_atm_pengelola')
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_atm_pengelola->read()->num_rows();
                $data['atm_pengelola'] = $this->m_atm_pengelola->read_pagination($config['per_page'], $data['start'])->result_array();

            }

        }
    
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "atm_pengelola";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_atm_pengelola');
        $this->session->unset_userdata('change_atm_pengelola');
        redirect('atm_pengelola');
    }

    public function hapus($kd)
    {
        $this->m_atm_pengelola->delete($kd);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('kode', 'Kode', 'required|trim|is_unique[atm_pengelola.kd_atm_pengelola]', [
			'is_unique' => 'Kode Pengelola sudah terdaftar!'
		]);
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('kode').form_error('nama') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $kode = $this->input->post('kode');
            $nama = $this->input->post('nama');
            //Array
            $data_atm_pengelola = [
                'kd_atm_pengelola' => $kode,
                'nama_atm_pengelola' => $nama,
                'created_atm_pengelola' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_atm_pengelola = $this->m_atm_pengelola->create($data_atm_pengelola);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('atm_pengelola');
        }
    }

    public function ubah($kd)
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $nama = $this->input->post('nama');
            //Array
            $data_atm_pengelola = [
                'nama_atm_pengelola' => $nama,
                'updated_atm_pengelola' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_atm_pengelola = $this->m_atm_pengelola->update($kd, $data_atm_pengelola);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('atm_pengelola');
        }
    }

    public function cetak()
    {
        //Ambil data
        $atm_pengelola = $this->m_atm_pengelola->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA PENGELOLA ATM', 0, 1, 'C');
              
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Kode Pengelola', 1, 0, 'C');
        $pdf->Cell(120, 6, 'Nama Pengelola', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($atm_pengelola as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['kd_atm_pengelola'], 1, 0);
            $pdf->Cell(120, 6, $key['nama_atm_pengelola'], 1, 0);
            $pdf->Cell(50, 6, $key['created_atm_pengelola'], 1, 0, 'C');
            $pdf->Cell(50, 6, $key['updated_atm_pengelola'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Digimon ATR WJY')
            ->setLastModifiedBy('Digimon ATR WJY')
            ->setTitle("Data Pengelola ATM")
            ->setSubject("Data Pengelola ATM")
            ->setDescription("Laporan Data Pengelola ATM")
            ->setKeywords("Data Pengelola ATM");

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B1', "Kode");
        $excel->setActiveSheetIndex(0)->setCellValue('C1', "Nama");
        $excel->setActiveSheetIndex(0)->setCellValue('D1', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('E1', "Updated");

        //ambil data antrian
        $data = $this->m_atm_pengelola->read()->result_array();
        $numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['kd_atm_pengelola']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_atm_pengelola']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['created_admin']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['updated_admin']);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Pengelola ATM");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Pengelola ATM.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

}