<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vendor_kebersihan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('email_user')) {

            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        $data['halaman'] = "vendor_kebersihan";
        // menuju index
        $this->load->view('index', $data);
        // $this->load->view('index');
    }

}