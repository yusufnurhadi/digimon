<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Performance extends CI_Controller
{

    // public function __construct()
    // {
    //     parent::__construct();

    //     if (!$this->session->userdata('email_user')) {

    //         $this->session->set_flashdata('error', 'Anda harus login dahulu ');
    //         redirect();
    //         die();
    //     }
    // }

    public function index()
    {
        // data
        $data['pref_cabang'] = $this->m_atm_cabang->read()->result_array();
        $data['pref_pengelola'] = $this->m_atm_pengelola->read()->result_array();
        // usage
        $data['pref_cabang'] = $this->m_atm_cabang->read()->result_array();
        $usage_2019 = [];
        $i=0;
        foreach ($data['pref_cabang'] as $cab) {
            // 2019
            $usage_2019 = $this->m_atm_usage->sum_where([
                'value_atm_usage !=' => '0',
                'year_atm_usage' => '2019',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $fbi_2019 = $this->m_atm_fbi->sum_where([
                'year_atm_fbi' => '2019',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $count_2019 = $this->m_atm_usage->read_where_group([
                'year_atm_usage' => '2019',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
            ], 'atm_usage.id_atm')->result_array();
            $data['pref_cabang'][$i]['usage_2019']=$usage_2019;
            $data['pref_cabang'][$i]['fbi_2019']=$fbi_2019;
            $data['pref_cabang'][$i]['count_2019']=count($count_2019);
            // 2020
            $usage_2020 = $this->m_atm_usage->sum_where([
                'year_atm_usage' => '2020',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $fbi_2020 = $this->m_atm_fbi->sum_where([
                'year_atm_fbi' => '2020',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $count_2020 = $this->m_atm_usage->read_where_group([
                'year_atm_usage' => '2020',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
            ], 'atm_usage.id_atm')->result_array();
            $data['pref_cabang'][$i]['usage_2020']=$usage_2020;
            $data['pref_cabang'][$i]['fbi_2020']=$fbi_2020;
            $data['pref_cabang'][$i]['count_2020']=count($count_2020);
            // 2021
            $usage_2021 = $this->m_atm_usage->sum_where([
                'year_atm_usage' => '2021',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $fbi_2021 = $this->m_atm_fbi->sum_where([
                'year_atm_fbi' => '2021',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
                ])->result_array();
            $count_2021 = $this->m_atm_usage->read_where_group([
                'year_atm_usage' => '2021',
                'atm.kd_atm_cabang' => $cab['kd_atm_cabang'],
            ], 'atm_usage.id_atm')->result_array();
            $data['pref_cabang'][$i]['usage_2021']=$usage_2021;
            $data['pref_cabang'][$i]['fbi_2021']=$fbi_2021;
            $data['pref_cabang'][$i]['count_2021']=count($count_2021);
            // i+1
            $i++;
        }

        // Usage
        $data['perf_atm_usage'] = $this->m_atm_usage->read()->result_array();
        $data['perf_atm_usage_2018'] = $this->m_atm_usage->read_where_usage(['year_atm_usage' => '2018'])->result_array();
        $data['perf_atm_usage_2019'] = $this->m_atm_usage->read_where_usage(['year_atm_usage' => '2019'])->result_array();
        $data['perf_atm_usage_2020'] = $this->m_atm_usage->read_where_usage(['year_atm_usage' => '2020'])->result_array();
        $data['perf_atm_usage_2021'] = $this->m_atm_usage->read_where_usage(['year_atm_usage' => '2021'])->result_array();
        // fbi
        $data['perf_atm_fbi'] = $this->m_atm_fbi->read()->result_array();
        $data['perf_atm_fbi_2018'] = $this->m_atm_fbi->read_where_fbi(['year_atm_fbi' => '2018'])->result_array();
        $data['perf_atm_fbi_2019'] = $this->m_atm_fbi->read_where_fbi(['year_atm_fbi' => '2019'])->result_array();
        $data['perf_atm_fbi_2020'] = $this->m_atm_fbi->read_where_fbi(['year_atm_fbi' => '2020'])->result_array();
        $data['perf_atm_fbi_2021'] = $this->m_atm_fbi->read_where_fbi(['year_atm_fbi' => '2021'])->result_array();
        // // tiket
        $data['halaman'] = "performance";
        // menuju index
        $this->load->view('index', $data);
        // $this->load->view('index');
    }

}