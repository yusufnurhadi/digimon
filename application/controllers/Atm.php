<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Atm extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('atm/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword') && $this->input->post('change')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_atm', $keyword);
            $this->session->set_userdata('change_atm', $change);

            $config['total_rows'] = $this->m_atm->read_like([
                $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
            ])->num_rows();
            $data['atm'] = $this->m_atm->read_like_pagination([
                $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
            ], $config['per_page'], $data['start'])->result_array();

        } else {

            if ($this->session->userdata('key_atm')) {

                $config['total_rows'] = $this->m_atm->read_like([
                    $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
                ])->num_rows();
                $data['atm'] = $this->m_atm->read_like_pagination([
                    $this->session->userdata('change_atm') => $this->session->userdata('key_atm')
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_atm->read()->num_rows();
                $data['atm'] = $this->m_atm->read_pagination($config['per_page'], $data['start'])->result_array();

            }

        }
    
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "atm";
        $this->load->view('index', $data);
    }

    public function detail($id)
    {
        $data['atm_usage'] = $this->m_atm_usage->read_where(['atm.id_atm' => $id])->result_array();
        $data['atm_usage_2018'] = $this->m_atm_usage->read_where_usage(['atm.id_atm' => $id, 'year_atm_usage' => '2018'])->result_array();
        $data['atm_usage_2019'] = $this->m_atm_usage->read_where_usage(['atm.id_atm' => $id, 'year_atm_usage' => '2019'])->result_array();
        $data['atm_usage_2020'] = $this->m_atm_usage->read_where_usage(['atm.id_atm' => $id, 'year_atm_usage' => '2020'])->result_array();
        $data['atm_usage_2021'] = $this->m_atm_usage->read_where_usage(['atm.id_atm' => $id, 'year_atm_usage' => '2021'])->result_array();
        $data['atm_fbi'] = $this->m_atm_fbi->read_where(['atm.id_atm' => $id])->result_array();
        $data['atm_fbi_2018'] = $this->m_atm_fbi->read_where_fbi(['atm.id_atm' => $id, 'year_atm_fbi' => '2018'])->result_array();
        $data['atm_fbi_2019'] = $this->m_atm_fbi->read_where_fbi(['atm.id_atm' => $id, 'year_atm_fbi' => '2019'])->result_array();
        $data['atm_fbi_2020'] = $this->m_atm_fbi->read_where_fbi(['atm.id_atm' => $id, 'year_atm_fbi' => '2020'])->result_array();
        $data['atm_fbi_2021'] = $this->m_atm_fbi->read_where_fbi(['atm.id_atm' => $id, 'year_atm_fbi' => '2021'])->result_array();
        $data['atm'] = $this->m_atm->read_where(['atm.id_atm' => $id])->row_array();
        $data['halaman'] = "detail_atm";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_atm');
        $this->session->unset_userdata('change_atm');
        redirect('atm');
    }

    public function hapus($id)
    {
        $atm = $this->m_atm->read_where(['id_atm'=>$id])->row_array();
        $this->m_atm->delete($atm['email_user']);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('id_atm', 'ID ATM', 'required|is_unique[atm.id_atm]', [
			'is_unique' => 'ID sudah terdaftar!'
		]);
        $this->form_validation->set_rules('atm_cabang', 'Cabang ATM', 'required');
        $this->form_validation->set_rules('atm_pengelola', 'Pengelola ATM', 'required');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('id_atm').form_error('atm_cabang').form_error('atm_pengelola').form_error('lokasi') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $id_atm = $this->input->post('id_atm');
            $atm_cabang = $this->input->post('atm_cabang');
            $atm_pengelola = $this->input->post('atm_pengelola');
            $lokasi = $this->input->post('lokasi');
            $alamat = $this->input->post('alamat');
            $kelurahan = $this->input->post('kelurahan');
            $kecamatan = $this->input->post('kecamatan');
            $kota = $this->input->post('kota');
            $provinsi = $this->input->post('provinsi');
            $lat = $this->input->post('lat');
            $lng = $this->input->post('lng');
            $jenis = $this->input->post('jenis');
            $merk_mesin = $this->input->post('merk_mesin');
            $jenis_mesin = $this->input->post('jenis_mesin');
            $sn_mesin = $this->input->post('sn_mesin');
            $denom = $this->input->post('denom');
            $jarkom = $this->input->post('jarkom');
            $merk_ups = $this->input->post('merk_ups');
            $sn_ups = $this->input->post('sn_ups');
            $merk_dome = $this->input->post('merk_dome');
            $sn_dome = $this->input->post('sn_dome');
            $merk_dvr = $this->input->post('merk_dvr');
            $sn_dvr = $this->input->post('sn_dvr');
            //Array
            $data_atm = [
                'id_atm' => $id_atm,
                'kd_atm_cabang' => $atm_cabang,
                'kd_atm_pengelola' => $atm_pengelola,
                'lokasi_atm' => $lokasi,
                'alamat_atm' => $alamat,
                'kelurahan_atm' => $kelurahan,
                'kecamatan_atm' => $kecamatan,
                'kota_atm' => $kota,
                'provinsi_atm' => $provinsi,
                'lat_atm' => $lat,
                'lng_atm' => $lng,
                'jenis_atm' => $jenis,
                'merk_mesin_atm' => $merk_mesin,
                'jenis_mesin_atm' => $jenis_mesin,
                'sn_mesin_atm' => $sn_mesin,
                'denom_atm' => $denom,
                'jarkom_atm' => $jarkom,
                'merk_ups_atm' => $merk_ups,
                'sn_ups_atm' => $sn_ups,
                'merk_dome_atm' => $merk_dome,
                'sn_dome_atm' => $sn_dome,
                'merk_dvr_atm' => $merk_dvr,
                'sn_dvr_atm' => $sn_dvr,
                'created_atm' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_atm = $this->m_atm->create($data_atm);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('atm');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('atm_cabang', 'Cabang ATM', 'required');
        $this->form_validation->set_rules('atm_pengelola', 'Pengelola ATM', 'required');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('atm_cabang').form_error('atm_pengelola').form_error('lokasi') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $atm_cabang = $this->input->post('atm_cabang');
            $atm_pengelola = $this->input->post('atm_pengelola');
            $lokasi = $this->input->post('lokasi');
            $alamat = $this->input->post('alamat');
            $kelurahan = $this->input->post('kelurahan');
            $kecamatan = $this->input->post('kecamatan');
            $kota = $this->input->post('kota');
            $provinsi = $this->input->post('provinsi');
            $lat = $this->input->post('lat');
            $lng = $this->input->post('lng');
            $jenis = $this->input->post('jenis');
            $merk_mesin = $this->input->post('merk_mesin');
            $jenis_mesin = $this->input->post('jenis_mesin');
            $sn_mesin = $this->input->post('sn_mesin');
            $denom = $this->input->post('denom');
            $jarkom = $this->input->post('jarkom');
            $merk_ups = $this->input->post('merk_ups');
            $sn_ups = $this->input->post('sn_ups');
            $merk_dome = $this->input->post('merk_dome');
            $sn_dome = $this->input->post('sn_dome');
            $merk_dvr = $this->input->post('merk_dvr');
            $sn_dvr = $this->input->post('sn_dvr');
            //Array
            $data_atm = [
                'kd_atm_cabang' => $atm_cabang,
                'kd_atm_pengelola' => $atm_pengelola,
                'lokasi_atm' => $lokasi,
                'alamat_atm' => $alamat,
                'kelurahan_atm' => $kelurahan,
                'kecamatan_atm' => $kecamatan,
                'kota_atm' => $kota,
                'provinsi_atm' => $provinsi,
                'lat_atm' => $lat,
                'lng_atm' => $lng,
                'jenis_atm' => $jenis,
                'merk_mesin_atm' => $merk_mesin,
                'jenis_mesin_atm' => $jenis_mesin,
                'sn_mesin_atm' => $sn_mesin,
                'denom_atm' => $denom,
                'jarkom_atm' => $jarkom,
                'merk_ups_atm' => $merk_ups,
                'sn_ups_atm' => $sn_ups,
                'merk_dome_atm' => $merk_dome,
                'sn_dome_atm' => $sn_dome,
                'merk_dvr_atm' => $merk_dvr,
                'sn_dvr_atm' => $sn_dvr,
                'updated_atm' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_atm = $this->m_atm->update($id, $data_atm);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('atm');
        }
    }

    public function unduh()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Digimon ATR WJY')
            ->setLastModifiedBy('Digimon ATR WJY')
            ->setTitle("Data ATM")
            ->setSubject("Data ATM")
            ->setDescription("Laporan Data ATM")
            ->setKeywords("Data ATM");

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B1', "ID ATM");
        $excel->setActiveSheetIndex(0)->setCellValue('C1', "Cabang");
        $excel->setActiveSheetIndex(0)->setCellValue('D1', "Pengelola");
        $excel->setActiveSheetIndex(0)->setCellValue('E1', "Lokasi");
        $excel->setActiveSheetIndex(0)->setCellValue('F1', "Alamat");
        $excel->setActiveSheetIndex(0)->setCellValue('G1', "Kelurahan");
        $excel->setActiveSheetIndex(0)->setCellValue('H1', "Kecamatan");
        $excel->setActiveSheetIndex(0)->setCellValue('I1', "Kota");
        $excel->setActiveSheetIndex(0)->setCellValue('J1', "Provinsi");
        $excel->setActiveSheetIndex(0)->setCellValue('K1', "Lattitude");
        $excel->setActiveSheetIndex(0)->setCellValue('L1', "Longitude");
        $excel->setActiveSheetIndex(0)->setCellValue('M1', "Jenis ATM");
        $excel->setActiveSheetIndex(0)->setCellValue('N1', "Merk Mesin");
        $excel->setActiveSheetIndex(0)->setCellValue('O1', "Jenis Mesin");
        $excel->setActiveSheetIndex(0)->setCellValue('P1', "SN Mesin");
        $excel->setActiveSheetIndex(0)->setCellValue('Q1', "Denom");
        $excel->setActiveSheetIndex(0)->setCellValue('R1', "Jarkom");
        $excel->setActiveSheetIndex(0)->setCellValue('S1', "Merk UPS");
        $excel->setActiveSheetIndex(0)->setCellValue('T1', "SN UPS");
        $excel->setActiveSheetIndex(0)->setCellValue('U1', "Merk Dome");
        $excel->setActiveSheetIndex(0)->setCellValue('V1', "SN Dome");
        $excel->setActiveSheetIndex(0)->setCellValue('W1', "Merk DVR");
        $excel->setActiveSheetIndex(0)->setCellValue('X1', "SN DVR");
        $excel->setActiveSheetIndex(0)->setCellValue('Y1', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('Z1', "Updated");

        //ambil data antrian
        $data = $this->m_atm->read()->result_array();
        $numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['id_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_atm_cabang']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['nama_atm_pengelola']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['lokasi_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['alamat_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $key['kelurahan_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $key['kecamatan_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $key['kota_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $key['provinsi_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $key['lat_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $key['lng_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $key['jenis_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $key['merk_mesin_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $key['jenis_mesin_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $key['sn_mesin_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $key['denom_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $key['jarkom_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $key['merk_ups_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $key['sn_ups_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, $key['merk_dome_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, $key['sn_dome_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, $key['merk_dvr_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('X' . $numrow, $key['sn_dvr_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('Y' . $numrow, $key['created_atm']);
            $excel->setActiveSheetIndex(0)->setCellValue('Z' . $numrow, $key['updated_atm']);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data ATM");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data ATM.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

}