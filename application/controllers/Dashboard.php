<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('email_user')) {

            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        if ($this->session->userdata('id_admin')) {
            $this->_admin();
        } else {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    private function _admin()
    {
        // master data
        $data['count_cabang'] = $this->m_atm_cabang->read()->num_rows();
        $data['count_pengelola'] = $this->m_atm_pengelola->read()->num_rows();
        $data['count_atm'] = $this->m_atm->read()->num_rows();
        $data['count_user'] = $this->m_user->read()->num_rows();
        // Usage & FBI
        $data['dash_atm_usage'] = $this->m_atm_usage->read()->result_array();
        $data['dash_atm_fbi'] = $this->m_atm_fbi->read()->result_array();
        for ($i=1; $i <= 12; $i++) { 
            // usage
            $data['dash_atm_usage_2019'][$i] = $this->m_atm_usage->avg_where([
                'value_atm_usage != ' => '0',
                'year_atm_usage' => '2019',
                'month_atm_usage' => $i,
                ])->result_array();
            $data['dash_atm_usage_2020'][$i] = $this->m_atm_usage->avg_where([
                'value_atm_usage != ' => '0',
                'year_atm_usage' => '2020',
                'month_atm_usage' => $i,
                ])->result_array();
            $data['dash_atm_usage_2021'][$i] = $this->m_atm_usage->avg_where([
                'value_atm_usage != ' => '0',
                'year_atm_usage' => '2021',
                'month_atm_usage' => $i,
                ])->result_array();
            // FBI
            $data['dash_atm_fbi_2019'][$i] = $this->m_atm_fbi->avg_where([
                'year_atm_fbi' => '2019',
                'month_atm_fbi' => $i,
                ])->result_array();
            $data['dash_atm_fbi_2020'][$i] = $this->m_atm_fbi->avg_where([
                'year_atm_fbi' => '2020',
                'month_atm_fbi' => $i,
                ])->result_array();
            $data['dash_atm_fbi_2021'][$i] = $this->m_atm_fbi->sum_where([
                'year_atm_fbi' => '2021',
                'month_atm_fbi' => $i,
                ])->result_array();
        }
        // // tiket
        // $data['ticket_new'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'created'])->num_rows();
        // $data['ticket_usulan'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'usulan'])->num_rows();
        // $data['ticket_spk'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'spk'])->num_rows();
        // $data['ticket_process'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'process'])->num_rows();
        // $data['ticket_pending'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'pending'])->num_rows();
        // $data['ticket_cancel'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'cancel'])->num_rows();
        // $data['ticket_finish'] = $this->m_ticket_header->read_where(['status_ticket_header' => 'finish', 'created_ticket_header >=' => date('Y-m-1 00:00:00')])->num_rows();
        // $data['ticket_history'] = $this->m_ticket_header->read_where(['created_ticket_header >=' => date('Y-m-1 00:00:00')])->num_rows();
        $data['halaman'] = "dashboard";
        // menuju index
        $this->load->view('index', $data);
        // $this->load->view('index');
    }

}