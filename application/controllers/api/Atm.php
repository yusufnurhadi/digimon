<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Atm extends REST_Controller
{

    public function client_post()
    {
        // deklarasi
        $id = $this->input->post('id');

        //cek
        $lokasi_baru = [];
        $client = $this->m_client->read_where(['id_client' => $id])->row_Array();
        $lokasi = $this->m_atm_lokasi->read_where([
            'atm_cabang.kd_wilayah' => $client['kd_wilayah'],
        ])->result_array();

        foreach ($lokasi as $lok) {
            //cek clean
            $cleaning = $this->m_cleaning->read_where([
                'cleaning.id_atm_lokasi' => $lok['id_atm_lokasi'],
                'tgl_cleaning' => date('Y-m-d'),
                'jadwal_cleaning !=' => 'bulanan',
                'status_cleaning' => 'clean',
            ])->result_array();
            $lok['status'] = count($cleaning);
            $lokasi_baru[] = $lok;
        }
        if (count($lokasi_baru) > 0) {
            //valid
            $this->response($lokasi_baru, REST_Controller::HTTP_OK);
        } else {
            //invalid
            $this->response([], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
