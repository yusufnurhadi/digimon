<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['level_user'])) {
			redirect('dashboard');
		}
	}

	public function index()
	{
		//jalur validasi
		$this->form_validation->set_rules('email', 'email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		//validasi
		if ($this->form_validation->run() == false) {
			//tidak valid
			$this->load->view('login');
		} else {
			//valid
			$this->_login();
		}
	}

	private function _login()
	{
		//ambil post
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		//cek model
		$user = $this->m_user->read_where(['email_user' => $email])->row_array();

		//validasi user
		if ($user) {
			//user valid
			//cek status user
			if ($user['is_active_user'] == 1) {
				//user aktif (is_active_user == 1)
				//validasi password
				if (password_verify($password, $user['password_user'])) {
					//password valid
					//alihkan ke masing-masing controller
					switch ($user['role_user']) {
						case 'admin':
							$admin = $this->m_admin->read_where(['admin.email_user' => $email])->row_array();
							$this->session->set_userdata($admin);
							$this->session->set_flashdata('success', 'Login Berhasil');
							redirect('dashboard');
							break;

						default:
							//default kosong
							break;
					}
				} else {
					//password tidak valid
					$this->session->set_flashdata('error', 'Password salah');
					redirect();
				}
			} else {
				//user tidak aktif (is_active_user == 0)
				$this->session->set_flashdata('error', 'Akun tidak aktif. Tunggu konfirmasi kami melalui email atau hubungi pihak Admin agar segera aktifasi akun anda');
				redirect();
			}
		} else {
			//user tidak valid
			$this->session->set_flashdata('error', 'Anda tidak terdaftar');
			redirect();
		}
	}
}
