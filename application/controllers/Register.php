<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['level_user'])) {
			if ($this->session->userdata['level_user'] == 'admin') {
				redirect('dashboard');
			} elseif ($this->session->userdata['level_user'] == '') {
				// redirect('c_admin');
			}
		}
	}

	public function index()
	{
		//jalur validasi
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('no_telp', 'No Telpon', 'required|trim');
		$this->form_validation->set_rules('password1', 'Password', 'required|trim');
		$this->form_validation->set_rules('password2', 'Password', 'required|trim');
		$this->form_validation->set_rules('akses', 'Akses', 'required|trim');

		//validasi
		if ($this->form_validation->run() == false) {
			//tidak valid
			$this->load->view('register');
		} else {
			//valid
			$this->_register();
		}
	}

	private function _register()
	{
		//jalur validasi
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'No Telpon', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email_user]', [
			'is_unique' => 'Email sudah terdaftar!'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|min_length[3]|matches[password1]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!'
		]);
		$this->form_validation->set_rules('akses', 'Akses', 'required|trim');
		//validasi
		if ($this->form_validation->run() == false) {
			//tidak valid
			$this->index();
			// redirect('c_register');
		} else {
			//valid
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$no_telp = $this->input->post('no_telp');
			$password1 = $this->input->post('password1');
			$akses = $this->input->post('akses');
			$atm_cabang = $this->input->post('atm_cabang');
			$atm_pengelola = $this->input->post('atm_pengelola');
			//Array User
			$data_user = [
				'email_user' => $email,
				'password_user' => password_hash($password1, PASSWORD_DEFAULT),
				'no_user' => $no_telp,
				'foto_user' => 'default.jpg',
				'role_user' => $akses,
				'is_active_user' => 0,
				'notif_user' => 1,
				'notif_email_user' => 1,
				'created_user' => date('Y-m-d H:i:s'),
			];

			switch ($akses) {
				case 'admin':
					//Simpan di database lewat model
					$this->m_user->create($data_user);
					$data_admin = [
						'email_user' => $email,
						'nama_admin' => $nama,
						'created_admin' => date('Y-m-d H:i:s'),
					];
					//simpan array
					$this->m_admin->create($data_admin);
					break;
				default:
					$this->index();
					break;
			}
			//berhasil
			$this->session->set_flashdata('success', 'Tunggu konfirmasi kami melalui email atau hubungi pihak Admin agar segera aktifasi akun anda');
			redirect();
		}
	}
}