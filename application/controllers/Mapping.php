<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapping extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        $data['title'] = "Mapping ATM";
        $data['atm'] = $this->m_atm->read_where(['lat_atm !=' => "0"])->result_array();
        $this->load->view('maps', $data);
    }
}