<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        // if (!$this->session->userdata('id_admin')) {

        //     $this->session->set_flashdata('error', 'Anda harus login dahulu ');
        //     redirect();
        //     die();
        // }
    }

    public function index()
    {
        //config pagination
        // $config['base_url'] = base_url('admin/index/');
        // $config['per_page'] = 10;
        // $data['start'] = $this->uri->segment(3);

        // //keyword
        // if ($this->input->post('keyword')) {

        //     $keyword = $this->input->post('keyword');
        //     $this->session->set_userdata('key_admin', $keyword);
        // } else {
        //     // $config['total_rows'] = $this->m_admin->read()->num_rows();
        //     // $data['admin'] = $this->m_admin->read_pagination($config['per_page'], $data['start'])->result_array();
        // }

        // $config['total_rows'] = $this->m_admin->read_like([
        //     'nama_admin' => $this->session->userdata('key_admin'),
        // ])->num_rows();
        // $data['admin'] = $this->m_admin->read_like_pagination(array('nama_admin' => $this->session->userdata('key_admin')), $config['per_page'], $data['start'])->result_array();
        // //inisialisasi
        // $this->pagination->initialize($config);

        // $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "user";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_admin');
        redirect('admin');
    }

    public function detail($id)
    {
        $data['admin'] = $this->m_admin->read_where(['id_admin' => $id])->row_array();
        $data['halaman'] = "detail_admin";
        $this->load->view('index', $data);
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.email_user]', [
            'is_unique' => 'Email sudah terdaftar',
        ]);

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('email'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_admin');
        } else {
            //valid
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = 'default.jpg';
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }
            //Array User
            $dataUser = [
                'email_user' => $email,
                'password_user' => password_hash(1234, PASSWORD_DEFAULT),
                'level_user' => 1,
                'is_active_user' => 1,
                'created_user' => date('Y-m-d H:i:s'),
            ];
            //Array Admin
            $dataAdmin = [
                'nama_admin' => $nama,
                'email_user' => $email,
                'foto_admin' => $foto,
                'created_admin' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanUser = $this->m_user->create($dataUser);
            $simpanAdmin = $this->m_admin->create($dataAdmin);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('admin');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('email'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_admin');
        } else {
            //valid
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $admin = $this->m_admin->read_where(['id_admin' => $id])->result_array();
            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $admin[0]['foto_admin'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }
            //Array User
            $dataUser = [
                'email_user' => $email,
                'updated_user' => date('Y-m-d H:i:s'),
            ];
            //Array Admin
            $dataAdmin = [
                'nama_admin' => $nama,
                'email_user' => $email,
                'updated_admin' => date('Y-m-d H:i:s'),
                'foto_admin' => $foto
            ];
            //Simpan di database lewat model
            $ubahUser = $this->m_user->update($admin[0]['email_user'], $dataUser);
            $ubahAdmin = $this->m_admin->update($id, $dataAdmin);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            echo "<script>javascript:history.back();</script>";
        }
    }

    public function hapus($id)
    {
        $admin = $this->m_admin->read_where(['id_admin' => $id])->row_array();
        $this->m_admin->delete($admin['email_user']);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function reset($id)
    {
        $admin = $this->m_admin->read_where(['id_admin' => $id])->row_array();
        $this->m_user->update($admin['email_user'], [
            'password_user' => password_hash(1234, PASSWORD_DEFAULT)
        ]);
        $this->session->set_flashdata('success', 'Password berhasil di reset ke 1234');
        echo "<script>javascript:history.back();</script>";
    }

    public function cetak()
    {
        //Ambil data
        $admin = $this->m_admin->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA ADMINISTRATOR', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(100, 6, 'Nama', 1, 0, 'C');
        $pdf->Cell(70, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(20, 6, 'Status', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($admin as $key) :
            if ($key['is_active_user'] == 1) {
                $status = "Aktif";
            } else {
                $status = "Non Aktif";
            }

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(100, 6, $key['nama_admin'], 1, 0);
            $pdf->Cell(70, 6, $key['email_user'], 1, 0);
            $pdf->Cell(20, 6, $status, 1, 0, 'C');
            $pdf->Cell(40, 6, $key['created_admin'], 1, 0, 'C');
            $pdf->Cell(40, 6, $key['updated_admin'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Pasti Clean')
            ->setLastModifiedBy('Pasti Clean')
            ->setTitle("Data Admin")
            ->setSubject("Data Admin")
            ->setDescription("Laporan Data Admin")
            ->setKeywords("Data Admin");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Set kolom A1
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA ADMINISTRATOR");
        // Set kolom B1
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CLEAN");
        $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('A2:F2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Nama");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Email");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "Status");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Updated");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);

        //ambil data antrian
        $data = $this->m_admin->read()->result_array();
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            if ($key['is_active_user'] == 1) {
                $status = "Aktif";
            } else {
                $status = "Non Antif";
            }

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['nama_admin']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['email_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $status);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['created_admin']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['updated_admin']);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom B

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Administrator");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Administrator.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

    public function aktifasi($id)
    {
        $ambil = $this->m_admin->read_where([
            'id_admin' => $id,
        ])->row_array();
        $this->m_user->update($ambil['email_user'], [
            'is_active_user' => 1,
            'updated_user' => date('Y-m-d H:i:s'),
        ]);
        $this->session->set_flashdata('success', 'User berhasil diaktifasi');
        echo "<script>javascript:history.back();</script>";
    }

    public function non_aktifasi($id)
    {
        $ambil = $this->m_admin->read_where([
            'id_admin' => $id,
        ])->row_array();
        $this->m_user->update($ambil['email_user'], [
            'is_active_user' => 0,
            'updated_user' => date('Y-m-d H:i:s'),
        ]);
        $this->session->set_flashdata('success', 'User berhasil dinonaktifasi');
        echo "<script>javascript:history.back();</script>";
    }
}
