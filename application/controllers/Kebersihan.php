<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kebersihan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('email_user')) {

            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('kebersihan/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword') && $this->input->post('change')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_kebersihan', $keyword);
            $this->session->set_userdata('change_kebersihan', $change);

            $config['total_rows'] = $this->m_kebersihan->read_like([
                $this->session->userdata('change_kebersihan') => $this->session->userdata('key_kebersihan'),
            ])->num_rows();
            $data['kebersihan'] = $this->m_kebersihan->read_like_pagination([
                $this->session->userdata('change_kebersihan') => $this->session->userdata('key_kebersihan'),
            ], $config['per_page'], $data['start'])->result_array();
        } else {

            if ($this->session->userdata('key_kebersihan')) {

                $config['total_rows'] = $this->m_kebersihan->read_like([
                    $this->session->userdata('change_kebersihan') => $this->session->userdata('key_kebersihan'),
                ])->num_rows();
                $data['kebersihan'] = $this->m_kebersihan->read_like_pagination([
                    $this->session->userdata('change_kebersihan') => $this->session->userdata('key_kebersihan')
                ], $config['per_page'], $data['start'])->result_array();
            } else {

                $config['total_rows'] = $this->m_kebersihan->read()->num_rows();
                $data['kebersihan'] = $this->m_kebersihan->read_pagination($config['per_page'], $data['start'])->result_array();
            }
        }

        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "kebersihan";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_kebersihan');
        $this->session->unset_userdata('change_kebersihan');
        redirect('kebersihan');
    }

    public function hapus($id)
    {
        $kebersihan = $this->m_kebersihan->read_where(['id_kebersihan' => $id])->row_array();
        $this->m_kebersihan->delete($kebersihan['email_user']);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email_user]', [
            'is_unique' => 'Email sudah terdaftar!'
        ]);
        $this->form_validation->set_rules('no_telp', 'No Telpon', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('email') . form_error('no_telp'));
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $no_telp = $this->input->post('no_telp');
            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = 'default.jpg';
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }
            //Array
            $data_user = [
                'email_user' => $email,
                'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
                'no_user' => $no_telp,
                'foto_user' => $foto,
                'role_user' => 'kebersihan',
                'is_active_user' => '1',
                'notif_user' => '1',
                'notif_email_user' => '1',
                'created_user' => date('Y-m-d H:i:s'),
            ];
            $data_kebersihan = [
                'email_user' => $email,
                'nama_kebersihan' => $nama,
                'created_kebersihan' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_user = $this->m_user->create($data_user);
            $simpan_kebersihan = $this->m_kebersihan->create($data_kebersihan);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('kebersihan');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('no_telp', 'No Telpon', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('notif', 'Notif', 'required');
        $this->form_validation->set_rules('notif_email', 'Notif Email', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('no_telp') . form_error('status') . form_error('status') . form_error('notif') . form_error('notif_email'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $nama = $this->input->post('nama');
            $no_telp = $this->input->post('no_telp');
            $status = $this->input->post('status');
            $notif = $this->input->post('notif');
            $notif_email = $this->input->post('notif_email');
            $kebersihan = $this->m_kebersihan->read_where(['id_kebersihan' => $id])->row_array();

            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $kebersihan['foto_user'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }

            //Array
            $data_kebersihan = [
                'nama_kebersihan' => $nama,
                'updated_kebersihan' => date('Y-m-d H:i:s'),
            ];
            $data_user = [
                'no_user' => $no_telp,
                'foto_user' => $foto,
                'is_active_user' => $status,
                'notif_user' => $notif,
                'notif_email_user' => $notif_email,
                'updated_user' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpan_kebersihan = $this->m_kebersihan->update($id, $data_kebersihan);
            $simpan_user = $this->m_user->update($kebersihan['email_user'], $data_user);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('kebersihan');
        }
    }

    public function reset($id)
    {
        $kebersihan = $this->m_kebersihan->read_where(['id_kebersihan' => $id])->row_array();
        $ubah = $this->m_user->update($kebersihan['email_user'], [
            'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
        ]);
        //berhasil
        $this->session->set_flashdata('success', 'Data berhasil direset');
        redirect('kebersihan');
    }

    public function cetak()
    {
        //Ambil data
        $kebersihan = $this->m_kebersihan->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA kebersihan', 0, 1, 'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Nama', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(50, 6, 'No Telpon', 1, 0, 'C');
        $pdf->Cell(15, 6, 'Status', 1, 0, 'C');
        $pdf->Cell(15, 6, 'Notif', 1, 0, 'C');
        $pdf->Cell(20, 6, 'Notif Email', 1, 0, 'C');
        $pdf->Cell(35, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(35, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($kebersihan as $key) :

            $status = "";
            if ($key['is_active_user'] == 0) {
                $status = "Register";
            } elseif ($key['is_active_user'] == 1) {
                $status = "Active";
            } elseif ($key['is_active_user'] == 2) {
                $status = "Block";
            }

            $notif = "";
            if ($key['notif_user'] == 0) {
                $notif = "Inactive";
            } elseif ($key['notif_user'] == 1) {
                $notif = "Active";
            }

            $notif_email = "";
            if ($key['notif_email_user'] == 0) {
                $notif_email = "Inactive";
            } elseif ($key['notif_email_user'] == 1) {
                $notif_email = "Active";
            }

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['nama_kebersihan'], 1, 0);
            $pdf->Cell(50, 6, $key['email_user'], 1, 0);
            $pdf->Cell(50, 6, $key['no_user'], 1, 0);
            $pdf->Cell(15, 6, $status, 1, 0);
            $pdf->Cell(15, 6, $notif, 1, 0);
            $pdf->Cell(20, 6, $notif_email, 1, 0);
            $pdf->Cell(35, 6, $key['created_kebersihan'], 1, 0, 'C');
            $pdf->Cell(35, 6, $key['updated_kebersihan'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Digimon ATR WJY')
            ->setLastModifiedBy('Digimon ATR WJY')
            ->setTitle("Data kebersihan")
            ->setSubject("Data kebersihan")
            ->setDescription("Laporan Data kebersihan")
            ->setKeywords("Data kebersihan");

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B1', "Nama");
        $excel->setActiveSheetIndex(0)->setCellValue('C1', "Email");
        $excel->setActiveSheetIndex(0)->setCellValue('D1', "No Telpon");
        $excel->setActiveSheetIndex(0)->setCellValue('E1', "Status");
        $excel->setActiveSheetIndex(0)->setCellValue('F1', "Notif");
        $excel->setActiveSheetIndex(0)->setCellValue('G1', "Notif Email");
        $excel->setActiveSheetIndex(0)->setCellValue('H1', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('I1', "Updated");

        //ambil data antrian
        $data = $this->m_kebersihan->read()->result_array();
        $numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['nama_kebersihan']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['email_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['no_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['is_active_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['notif_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $key['notif_email_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $key['created_kebersihan']);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $key['updated_kebersihan']);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data kebersihan");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data kebersihan.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }
}
