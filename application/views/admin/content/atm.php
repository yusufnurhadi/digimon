<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">ATM</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">ATM</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <!-- <a href="<?= base_url('atm/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to
                    PDF</a> -->
                <a href="<?= base_url('atm/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to
                    Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/atm') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search .."
                                    autocomplete="off" autofocus="" value="<?= $this->session->userdata('key_atm') ?>">
                                <select name="change" class="form-control">
                                    <option value="atm.id_atm"
                                        <?php if($this->session->userdata('change_atm')=="atm.id_atm") echo "selected"; ?>>
                                        ID ATM</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('atm/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Cabang ATM</th>
                            <th nowrap>Pengelola ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Alamat</th>
                            <th nowrap>Kelurahan</th>
                            <th nowrap>Kecamatan</th>
                            <th nowrap>Kota</th>
                            <th nowrap>Provinsi</th>
                            <th nowrap>Lattitude</th>
                            <th nowrap>Longitude</th>
                            <th nowrap>Jenis ATM</th>
                            <th nowrap>Merk Mesin</th>
                            <th nowrap>Jenis Mesin</th>
                            <th nowrap>SN Mesin</th>
                            <th nowrap>Denom</th>
                            <th nowrap>Jarkom</th>
                            <th nowrap>Merk UPS</th>
                            <th nowrap>SN UPS</th>
                            <th nowrap>Merk Dome</th>
                            <th nowrap>SN Dome</th>
                            <th nowrap>Merk DVR</th>
                            <th nowrap>SN DVR</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if (empty($atm)) : ?>

                        <tr>
                            <td colspan="27" class="text-center">Tidak ada data</td>
                        </tr>

                        <?php endif; ?>

                        <?php foreach ($atm as $key) : ?>

                        <tr>
                            <td><?= ++$start; ?></td>
                            <td nowrap><?= $key['id_atm'] ?></td>
                            <td nowrap><?= $key['nama_atm_cabang'] ?></td>
                            <td nowrap><?= $key['nama_atm_pengelola'] ?></td>
                            <td nowrap><?= $key['lokasi_atm'] ?></td>
                            <td nowrap><?= $key['alamat_atm'] ?></td>
                            <td nowrap><?= $key['kelurahan_atm'] ?></td>
                            <td nowrap><?= $key['kecamatan_atm'] ?></td>
                            <td nowrap><?= $key['kota_atm'] ?></td>
                            <td nowrap><?= $key['provinsi_atm'] ?></td>
                            <td nowrap><?= $key['lat_atm'] ?></td>
                            <td nowrap><?= $key['lng_atm'] ?></td>
                            <td nowrap><?= $key['jenis_atm'] ?></td>
                            <td nowrap><?= $key['merk_mesin_atm'] ?></td>
                            <td nowrap><?= $key['jenis_mesin_atm'] ?></td>
                            <td nowrap><?= $key['sn_mesin_atm'] ?></td>
                            <td nowrap><?= $key['denom_atm'] ?></td>
                            <td nowrap><?= $key['jarkom_atm'] ?></td>
                            <td nowrap><?= $key['merk_ups_atm'] ?></td>
                            <td nowrap><?= $key['sn_ups_atm'] ?></td>
                            <td nowrap><?= $key['merk_dome_atm'] ?></td>
                            <td nowrap><?= $key['sn_dome_atm'] ?></td>
                            <td nowrap><?= $key['merk_dvr_atm'] ?></td>
                            <td nowrap><?= $key['sn_dvr_atm'] ?></td>
                            <td nowrap><?= $key['created_atm'] ?></td>
                            <td nowrap><?= $key['updated_atm'] ?></td>
                            <td nowrap>
                                <a href="<?= base_url('atm/detail/' . $key['id_atm']) ?>" class="btn btn-xs btn-primary"
                                    title="Detail">Detail</a>
                                <a href="#" class="btn btn-xs btn-warning" data-toggle="modal"
                                    data-target="#modal-ubah-<?= $key['id_atm'] ?>" title="Ubah">Ubah</a>
                                <a href="<?= base_url('atm/hapus/' . $key['id_atm']) ?>" class="btn btn-xs btn-danger"
                                    title="Hapus"
                                    onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                            </td>
                        </tr>

                        <!-- Modal Edit -->
                        <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['id_atm'] ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Ubah Data ATM</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?= form_open_multipart('atm/ubah/' . $key['id_atm']); ?>
                                        <div class="form-group">
                                            <label>ID ATM *</label>
                                            <input type="text" name="id_atm" value="<?= $key['id_atm'] ?>"
                                                class="form-control" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Lokasi *</label>
                                            <input type="text" name="lokasi" value="<?= $key['lokasi_atm'] ?>"
                                                class="form-control">
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($atm); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data ATM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('atm/tambah'); ?>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label>No Telpon *</label>
                    <input type="text" name="no_telp" class="form-control" placeholder="No Telpon" required>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control">
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>