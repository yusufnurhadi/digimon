<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detail ATM</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Detail ATM</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <!-- <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a> -->
                <!-- <a href="<?= base_url('atm/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to
                    PDF</a> -->
                <!-- <a href="<?= base_url('atm/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to
                    Excel</a> -->
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Header</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Cabang ATM</th>
                            <th nowrap>Pengelola ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Alamat</th>
                            <th nowrap>Kelurahan</th>
                            <th nowrap>Kecamatan</th>
                            <th nowrap>Kota</th>
                            <th nowrap>Provinsi</th>
                            <th nowrap>Lattitude</th>
                            <th nowrap>Longitude</th>
                            <th nowrap>Jenis ATM</th>
                            <th nowrap>Merk Mesin</th>
                            <th nowrap>Jenis Mesin</th>
                            <th nowrap>SN Mesin</th>
                            <th nowrap>Denom</th>
                            <th nowrap>Jarkom</th>
                            <th nowrap>Merk UPS</th>
                            <th nowrap>SN UPS</th>
                            <th nowrap>Merk Dome</th>
                            <th nowrap>SN Dome</th>
                            <th nowrap>Merk DVR</th>
                            <th nowrap>SN DVR</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td nowrap><?= $atm['id_atm'] ?></td>
                            <td nowrap><?= $atm['nama_atm_cabang'] ?></td>
                            <td nowrap><?= $atm['nama_atm_pengelola'] ?></td>
                            <td nowrap><?= $atm['lokasi_atm'] ?></td>
                            <td nowrap><?= $atm['alamat_atm'] ?></td>
                            <td nowrap><?= $atm['kelurahan_atm'] ?></td>
                            <td nowrap><?= $atm['kecamatan_atm'] ?></td>
                            <td nowrap><?= $atm['kota_atm'] ?></td>
                            <td nowrap><?= $atm['provinsi_atm'] ?></td>
                            <td nowrap><?= $atm['lat_atm'] ?></td>
                            <td nowrap><?= $atm['lng_atm'] ?></td>
                            <td nowrap><?= $atm['jenis_atm'] ?></td>
                            <td nowrap><?= $atm['merk_mesin_atm'] ?></td>
                            <td nowrap><?= $atm['jenis_mesin_atm'] ?></td>
                            <td nowrap><?= $atm['sn_mesin_atm'] ?></td>
                            <td nowrap><?= $atm['denom_atm'] ?></td>
                            <td nowrap><?= $atm['jarkom_atm'] ?></td>
                            <td nowrap><?= $atm['merk_ups_atm'] ?></td>
                            <td nowrap><?= $atm['sn_ups_atm'] ?></td>
                            <td nowrap><?= $atm['merk_dome_atm'] ?></td>
                            <td nowrap><?= $atm['sn_dome_atm'] ?></td>
                            <td nowrap><?= $atm['merk_dvr_atm'] ?></td>
                            <td nowrap><?= $atm['sn_dvr_atm'] ?></td>
                            <td nowrap><?= $atm['created_atm'] ?></td>
                            <td nowrap><?= $atm['updated_atm'] ?></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <div class="row">
            <div class="col-md-12">
                <!-- AREA CHART -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Grafik Usage</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="usage"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- AREA CHART -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Grafik FBI</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="fbi"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Installasi</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Tangal</th>
                            <th nowrap>Vendor</th>
                            <th nowrap>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="5" class="text-center">Tidak ada data</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Relokasi</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Lokasi Lama</th>
                            <th nowrap>Lokasi Baru</th>
                            <th nowrap>Tangal</th>
                            <th nowrap>Vendor</th>
                            <th nowrap>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="6" class="text-center">Tidak ada data</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Replacement</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>ID Lama</th>
                            <th nowrap>ID Baru</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Tangal</th>
                            <th nowrap>Vendor</th>
                            <th nowrap>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="6" class="text-center">Tidak ada data</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">ATM Problem</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>No Tiket</th>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Pelapor</th>
                            <th nowrap>Problem</th>
                            <th nowrap>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="6" class="text-center">Tidak ada data</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Room Problem</h3>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>No Tiket</th>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Pelapor</th>
                            <th nowrap>Problem</th>
                            <th nowrap>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td colspan="6" class="text-center">Tidak ada data</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data ATM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('atm/tambah'); ?>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label>No Telpon *</label>
                    <input type="text" name="no_telp" class="form-control" placeholder="No Telpon" required>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control">
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>