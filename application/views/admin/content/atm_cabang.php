<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cabang ATM</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Cabang ATM</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('atm_cabang/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data
                    to
                    PDF</a>
                <a href="<?= base_url('atm_cabang/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export
                    Data to
                    Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/atm_cabang') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search .."
                                    autocomplete="off" autofocus=""
                                    value="<?= $this->session->userdata('key_atm_cabang') ?>">
                                <select name="change" class="form-control">
                                    <option value="atm_cabang.kode_atm_cabang"
                                        <?php if($this->session->userdata('change_atm_cabang')=="atm_cabang.kode_atm_cabang") echo "selected"; ?>>
                                        Kode</option>
                                    <option value="atm_cabang.nama_atm_cabang"
                                        <?php if($this->session->userdata('change_atm_cabang')=="atm_cabang.nama_atm_cabang") echo "selected"; ?>>
                                        Nama</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('atm_cabang/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if (empty($atm_cabang)) : ?>

                        <tr>
                            <td colspan="6" class="text-center">Tidak ada data</td>
                        </tr>

                        <?php endif; ?>

                        <?php foreach ($atm_cabang as $key) : ?>

                        <tr>
                            <td><?= ++$start; ?></td>
                            <td nowrap><?= $key['kd_atm_cabang'] ?></td>
                            <td nowrap><?= $key['nama_atm_cabang'] ?></td>
                            <td nowrap><?= $key['created_atm_cabang'] ?></td>
                            <td nowrap><?= $key['updated_atm_cabang'] ?></td>
                            <td nowrap>
                                <a href="#" class="btn btn-xs btn-warning" data-toggle="modal"
                                    data-target="#modal-ubah-<?= $key['kd_atm_cabang'] ?>" title="Ubah">Ubah</a>
                                <a href="<?= base_url('atm_cabang/hapus/' . $key['kd_atm_cabang']) ?>"
                                    class="btn btn-xs btn-danger" title="Hapus"
                                    onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                            </td>
                        </tr>

                        <!-- Modal Edit -->
                        <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['kd_atm_cabang'] ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Ubah Data Cabang ATM</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?= form_open_multipart('atm_cabang/ubah/' . $key['kd_atm_cabang']); ?>
                                        <div class="form-group">
                                            <label>Kode *</label>
                                            <input type="text" name="kode" value="<?= $key['kd_atm_cabang'] ?>"
                                                class="form-control" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama *</label>
                                            <input type="text" name="nama" value="<?= $key['nama_atm_cabang'] ?>"
                                                class="form-control" required>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($atm_cabang); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Cabang ATM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('atm_cabang/tambah'); ?>
                <div class="form-group">
                    <label>Kode Cabang *</label>
                    <input type="text" name="kode" class="form-control" placeholder="Kode Cabang" required>
                </div>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>