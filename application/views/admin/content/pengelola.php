<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Admin Pengelola</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Admin Pengelola</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('pengelola/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to
                    PDF</a>
                <a href="<?= base_url('pengelola/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to
                    Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/pengelola') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search .." autocomplete="off" autofocus="" value="<?= $this->session->userdata('key_pengelola') ?>">
                                <select name="change" class="form-control">
                                    <option value="pengelola.nama_pengelola" <?php if ($this->session->userdata('change_pengelola') == "pengelola.nama_pengelola") echo "selected"; ?>>
                                        Nama</option>
                                    <option value="user.email_user" <?php if ($this->session->userdata('change_pengelola') == "user.email_user") echo "selected"; ?>>
                                        Email</option>
                                    <option value="user.no_user" <?php if ($this->session->userdata('change_pengelola') == "user.no_user") echo "selected"; ?>>
                                        No Telpon</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('pengelola/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>pengelola</th>
                            <th nowrap>No Telpon</th>
                            <th>Status</th>
                            <th>Notif</th>
                            <th nowrap>Notif Email</th>
                            <th>Foto</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if (empty($pengelola)) : ?>

                            <tr>
                                <td colspan="12" class="text-center">Tidak ada data</td>
                            </tr>

                        <?php endif; ?>

                        <?php foreach ($pengelola as $key) : ?>

                            <tr>
                                <td><?= ++$start; ?></td>
                                <td nowrap><?= $key['nama_pengelola'] ?></td>
                                <td nowrap><?= $key['email_user'] ?></td>
                                <td nowrap><?= $key['nama_atm_pengelola'] ?></td>
                                <td nowrap><?= $key['no_user'] ?></td>
                                <td>
                                    <?php
                                    if ($key['is_active_user'] == 0) {
                                        echo "<span class='badge bg-primary'>Register</span>";
                                    } elseif ($key['is_active_user'] == 1) {
                                        echo "<span class='badge bg-success'>Active</span>";
                                    } else {
                                        echo "<span class='badge bg-danger'>Block</span>";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($key['notif_user'] == 1) {
                                        echo "<span class='badge bg-success'>Active</span>";
                                    } elseif ($key['notif_user'] == 0) {
                                        echo "<span class='badge bg-danger'>Inactive</span>";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($key['notif_email_user'] == 1) {
                                        echo "<span class='badge bg-success'>Active</span>";
                                    } elseif ($key['notif_email_user'] == 0) {
                                        echo "<span class='badge bg-danger'>Inactive</span>";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-foto-<?= $key['id_pengelola'] ?>">Lihat
                                    </a>
                                </td>
                                <td nowrap><?= $key['created_user'] ?></td>
                                <td nowrap><?= $key['updated_user'] ?></td>
                                <td nowrap>
                                    <!-- <a href="<?= base_url('pengelola/detail/' . $key['id_pengelola']) ?>"
                                    class="btn btn-xs btn-primary" title="Detail">Detail</a> -->
                                    <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-ubah-<?= $key['id_pengelola'] ?>" title="Ubah">Ubah</a>
                                    <a href="<?= base_url('pengelola/hapus/' . $key['id_pengelola']) ?>" class="btn btn-xs btn-danger" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                                    <a href="<?= base_url('pengelola/reset/' . $key['id_pengelola']) ?>" class="btn btn-xs btn-secondary" title="Reset Password" onclick="return confirm('Apakah anda yakin ingin mereset password ke 1234 ?')">Reset
                                        Password</a>
                                </td>
                            </tr>
                            <!-- Modal Foto -->
                            <div class="modal fade bd-example-modal-lg" data-backdrop="static" id="modal-foto-<?= $key['id_pengelola'] ?>">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?= $key['nama_pengelola'] ?></h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <img style="max-height: 300px; max-width: 300px;" src="<?= base_url('img/profile/' . $key['foto_user']) ?>" class="img-circle elevation-2">
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Edit -->
                            <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['id_pengelola'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Ubah Data Kantor pengelola</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?= form_open_multipart('pengelola/ubah/' . $key['id_pengelola']); ?>
                                            <div class="form-group">
                                                <label>Nama *</label>
                                                <input type="text" name="nama" value="<?= $key['nama_pengelola'] ?>" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Email *</label>
                                                <input type="text" name="email" value="<?= $key['email_user'] ?>" class="form-control" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>No Telpon *</label>
                                                <input type="text" name="no_telp" value="<?= $key['no_user'] ?>" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Status *</label>
                                                <select name="status" class="form-control">
                                                    <option value="0" <?php if ($key['is_active_user'] == "0") echo "selected"; ?>>
                                                        Register</option>
                                                    <option value="1" <?php if ($key['is_active_user'] == "1") echo "selected"; ?>>
                                                        Active</option>
                                                    <option value="2" <?php if ($key['is_active_user'] == "2") echo "selected"; ?>>
                                                        Block</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notif *</label>
                                                <select name="notif" class="form-control">
                                                    <option value="0" <?php if ($key['notif_user'] == "0") echo "selected"; ?>>
                                                        Inactive</option>
                                                    <option value="1" <?php if ($key['notif_user'] == "1") echo "selected"; ?>>
                                                        Active</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notif Email *</label>
                                                <select name="notif_email" class="form-control">
                                                    <option value="0" <?php if ($key['notif_email_user'] == "0") echo "selected"; ?>>
                                                        Inactive</option>
                                                    <option value="1" <?php if ($key['notif_email_user'] == "1") echo "selected"; ?>>
                                                        Active</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Foto</label><br>
                                                <img style="max-height: 100px; max-width: 100px;" src="<?= base_url('img/profile/' . $key['foto_user']) ?>">
                                                <input type="file" name="foto" class="form-control">
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($pengelola); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Kantor pengelola</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('pengelola/tambah'); ?>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label>Kode *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label>No Telpon *</label>
                    <input type="text" name="no_telp" class="form-control" placeholder="No Telpon" required>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control">
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>