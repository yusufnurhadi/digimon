<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="#">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Users</span>
                                <span class="info-box-number"><?= $count_user; ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm_cabang/') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-warehouse"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Cabang</span>
                                <span class="info-box-number"><?= $count_cabang; ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm_pengelola/') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-map"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Pengelola</span>
                                <span class="info-box-number"><?= $count_pengelola; ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="#">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-map-marked-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Vendor Kebersihan</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm/') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-university"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">ATM</span>
                                <span class="info-box-number"><?= $count_atm; ?></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

            </div>
            <!-- /.row -->

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0 text-dark">Activity</h5>
                </div><!-- /.col -->
            </div>

            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('activity/pemasangan') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-secondary elevation-1"><i
                                    class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Pemasangan</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('activity/relokasi') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-secondary elevation-1"><i
                                    class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Relokasi</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('activity/replacement') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-secondary elevation-1"><i
                                    class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Replacement</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
            </div>

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0 text-dark">ATM Problem</h5>
                </div><!-- /.col -->
            </div>

            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm_problem/new') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">New</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm_problem/proses') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Proses</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('atm_problem/history') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">History</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
            </div>

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0 text-dark">Room Problem</h5>
                </div><!-- /.col -->
            </div>

            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('room_problem/new') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">New</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('room_problem/proses') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">Proses</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('room_problem/history') ?>">
                        <div class="info-box">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-ticket-alt"></i></span>

                            <div class="info-box-content text-dark">
                                <span class="info-box-text">History</span>
                                <span class="info-box-number">0</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </a>
                </div>
                <!-- /.col -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Usage & FBI </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tahun</th>
                                        <th nowrap>Kelolaan</th>
                                        <th nowrap>Total Usage</th>
                                        <th nowrap>Total FBI</th>
                                        <th nowrap>Average Usage</th>
                                        <th nowrap>Average FBI</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <!-- <tr>
                                        <td>2018</td>
                                        <td><?= number_format($cab['count_2018']) ?></td>
                                        <td><?= number_format($cab['usage_2018'][0]['value_atm_usage']) ?></td>
                                        <td><?= number_format($cab['fbi_2018'][0]['value_atm_fbi']) ?></td>
                                        <td><?= number_format($cab['usage_2018'][0]['value_atm_usage']/$cab['count_2018']/12) ?>
                                        </td>
                                        <td><?= number_format($cab['fbi_2018'][0]['value_atm_fbi']/$cab['count_2018']/12) ?>
                                        </td>
                                    </tr> -->

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Grafik Usage</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tahun</th>
                                        <th nowrap>Januari</th>
                                        <th nowrap>Februari</th>
                                        <th nowrap>Maret</th>
                                        <th nowrap>April</th>
                                        <th nowrap>Mei</th>
                                        <th nowrap>Juni</th>
                                        <th nowrap>Juli</th>
                                        <th nowrap>Agustus</th>
                                        <th nowrap>September</th>
                                        <th nowrap>Oktober</th>
                                        <th nowrap>November</th>
                                        <th nowrap>Desember</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if (isset($dash_atm_usage_2019)) : ?>

                                    <tr>
                                        <td>2019</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_usage_2019[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_usage_2019[$i][0]['value_atm_usage']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>
                                    <?php if (isset($dash_atm_usage_2020)) : ?>

                                    <tr>
                                        <td>2020</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_usage_2020[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_usage_2020[$i][0]['value_atm_usage']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>
                                    <?php if (isset($dash_atm_usage_2021)) : ?>

                                    <tr>
                                        <td>2021</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_usage_2021[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_usage_2021[$i][0]['value_atm_usage']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="usage_tahunan"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Grafik FBI</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tahun</th>
                                        <th nowrap>Januari</th>
                                        <th nowrap>Februari</th>
                                        <th nowrap>Maret</th>
                                        <th nowrap>April</th>
                                        <th nowrap>Mei</th>
                                        <th nowrap>Juni</th>
                                        <th nowrap>Juli</th>
                                        <th nowrap>Agustus</th>
                                        <th nowrap>September</th>
                                        <th nowrap>Oktober</th>
                                        <th nowrap>November</th>
                                        <th nowrap>Desember</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if (isset($dash_atm_fbi_2019)) : ?>

                                    <tr>
                                        <td>2019</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_fbi_2019[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_fbi_2019[$i][0]['value_atm_fbi']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>
                                    <?php if (isset($dash_atm_fbi_2020)) : ?>

                                    <tr>
                                        <td>2020</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_fbi_2020[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_fbi_2020[$i][0]['value_atm_fbi']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>
                                    <?php if (isset($dash_atm_fbi_2021)) : ?>

                                    <tr>
                                        <td>2021</td>

                                        <?php
                                        for ($i = 1; $i <= 12; $i++) : 
                                            if ($dash_atm_fbi_2021[$i]) {
                                                
                                                echo "<td>";
                                                echo number_format($dash_atm_fbi_2021[$i][0]['value_atm_fbi']);
                                                echo "</td>";

                                                } else {

                                                    echo '0';
                                                    
                                            }
                                        endfor;
                                        ?>

                                    </tr>

                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="fbi_tahunan"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->