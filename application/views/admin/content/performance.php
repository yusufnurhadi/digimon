<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Performance</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Performance</li>
                    </ol>
                </div><!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <?php foreach($pref_cabang as $cab) : ?>

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5 class="m-0 text-dark">Cabang <?= ucfirst(strtolower($cab['nama_atm_cabang'])) ?></h5>
                </div><!-- /.col -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Usage & FBI Cabang
                                <?= ucfirst(strtolower($cab['nama_atm_cabang'])) ?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tahun</th>
                                        <th nowrap>Kelolaan</th>
                                        <th nowrap>Total Usage</th>
                                        <th nowrap>Total FBI</th>
                                        <th nowrap>Average Usage</th>
                                        <th nowrap>Average FBI</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if($cab['count_2019'] != 0) : ?>

                                    <tr>
                                        <td>2019</td>
                                        <td><?= number_format($cab['count_2019']) ?></td>
                                        <td><?= number_format($cab['usage_2019'][0]['value_atm_usage']) ?></td>
                                        <td><?= number_format($cab['fbi_2019'][0]['value_atm_fbi']) ?></td>
                                        <td><?= number_format($cab['usage_2019'][0]['value_atm_usage']/$cab['count_2019']/12) ?>
                                        </td>
                                        <td><?= number_format($cab['fbi_2019'][0]['value_atm_fbi']/$cab['count_2019']/12) ?>
                                        </td>
                                    </tr>

                                    <?php endif; ?>
                                    <?php if($cab['count_2020'] != 0) : ?>

                                    <tr>
                                        <td>2020</td>
                                        <td><?= number_format($cab['count_2020']) ?></td>
                                        <td><?= number_format($cab['usage_2020'][0]['value_atm_usage']) ?></td>
                                        <td><?= number_format($cab['fbi_2020'][0]['value_atm_fbi']) ?></td>
                                        <td><?= number_format($cab['usage_2020'][0]['value_atm_usage']/$cab['count_2020']/12) ?>
                                        </td>
                                        <td><?= number_format($cab['fbi_2020'][0]['value_atm_fbi']/$cab['count_2020']/12) ?>
                                        </td>
                                    </tr>

                                    <?php endif; ?>
                                    <?php if($cab['count_2021'] != 0) : ?>

                                    <tr>
                                        <td>2021</td>
                                        <td><?= number_format($cab['count_2021']) ?></td>
                                        <td><?= number_format($cab['usage_2021'][0]['value_atm_usage']) ?></td>
                                        <td><?= number_format($cab['fbi_2021'][0]['value_atm_fbi']) ?></td>
                                        <td><?= number_format($cab['usage_2021'][0]['value_atm_usage']/$cab['count_2021']/12) ?>
                                        </td>
                                        <td><?= number_format($cab['fbi_2021'][0]['value_atm_fbi']/$cab['count_2021']/12) ?>
                                        </td>
                                    </tr>

                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Usage Cabang <?= ucfirst(strtolower($cab['nama_atm_cabang'])) ?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="usage_tahunan"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">FBI Cabang <?= ucfirst(strtolower($cab['nama_atm_cabang'])) ?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="fbi_tahunan"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach; ?>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->