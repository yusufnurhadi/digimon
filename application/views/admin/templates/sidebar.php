<?php

// if ($halaman == "ticket") $halaman1 = $halaman.'_'.$tittle;
  
function nav_item($text, $icon, $halaman, $page, $url) {

    if (isset($halaman1)) {
        $halaman=$halaman1;
    }

    $hasil = '';

    if (isset($halaman))
        if ($halaman == $page)
        $hasil = 'active';
       
    echo "<li class='nav-item'>";
    echo "<a href='".base_url($url)."' class='nav-link $hasil'>";
    echo "<i class='nav-icon fas fa-$icon'></i>";
    echo "<p>$text</p>";
    echo "</a>";
    echo "</li>";

}

function nav_treeview($text, $icon, $halaman, $array) {

    $hasil = '';
    $hasil2 = '';


    for ($i=0; $i < count($array); $i++) {

        if (isset($halaman))
            if ($halaman == $array[$i]['halaman'])
            {
                $hasil = 'active';
                $hasil2 = 'menu-open';
            }
    }

    echo "<li class='nav-item has-treeview $hasil2'>";
    echo "<a href='#' class='nav-link $hasil'>";
    echo "<i class='nav-icon fas fa-$icon'></i>";
    echo "<p>";
    echo "$text";
    echo "<i class='right fas fa-angle-left'></i>";
    echo "</p>";
    echo "</a>";
    echo "<ul class='nav nav-treeview'>";

    for ($i=0; $i < count($array); $i++) {

        $hasil = '';

        if (isset($halaman))
            if ($halaman == $array[$i]['halaman'])
                $hasil = 'active';

        echo "<li class='nav-item'>";
        echo "<a href='".base_url($array[$i]['url'])."' class='nav-link $hasil'>";
        echo "<i class='far fa-circle nav-icon'></i>";
        echo "<p>".$array[$i]['kd']."</p>";
        echo "</a>";
        echo "</li>";

    }

    echo "</ul>";
    echo "</li>";

}
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="<?= base_url('img/logo/46.png') ?>" alt="digimon Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <!-- <img src="<?= base_url('assets/') ?>dist/img/AdminLTELogo.png" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
        <span class="brand-text font-weight-light">DIGIMON ATR WJY</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <!-- Menampilkan foto tergantung user -->
                <img src="<?= base_url('img/profile/'.$this->session->userdata('foto_user')) ?>"
                    class="img-circle elevation-2" alt="Foto Profile">
                <!-- Akhir menampilkan foto tergantung user -->
            </div>
            <div class="info">
                <!-- Menampilkan nama tergantung user -->
                <a href="#" class="d-block"><?= $this->session->userdata('nama_admin') ?></a>
                <!-- Akhir menampilkan nama tergantung user -->
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-compact" data-widget="treeview"
                role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <?php

                    nav_item('Dashboard', 'home', $halaman, 'dashboard', 'dashboard/');
                    nav_item('Performance', 'tachometer-alt', $halaman, 'performance', 'performance/');

                ?>

                <li class="nav-header">MASTER DATA</li>

                <?php

                    nav_treeview('Users', 'users', $halaman, [

                        [
                            'kd' => 'Administrator',
                            'url' => 'admin/',
                            'halaman' => 'admin',
                        ],
                        [
                            'kd' => 'Kantor Cabang',
                            'url' => 'cabang/',
                            'halaman' => 'cabang',
                        ],
                        [
                            'kd' => 'Checker Cabang',
                            'url' => 'checker/',
                            'halaman' => 'checker',
                        ],
                        [
                            'kd' => 'Admin Pengelola',
                            'url' => 'pengelola/',
                            'halaman' => 'pengelola',
                        ],
                        [
                            'kd' => 'Teknisi Pengelola',
                            'url' => 'pengelola_teknisi/',
                            'halaman' => 'pengelola_teknisi',
                        ],
                        [
                            'kd' => 'Admin Kebersihan',
                            'url' => 'kebersihan/',
                            'halaman' => 'kebersihan',
                        ],
                        [
                            'kd' => 'Operator Kebersihan',
                            'url' => 'kebersihan_operator/',
                            'halaman' => 'kebersihan_operator',
                        ],

                    ]);

                    nav_item('Cabang', 'university', $halaman, 'atm_cabang', 'atm_cabang/');
                    nav_item('Pengelola', 'building', $halaman, 'atm_pengelola', 'atm_pengelola/');
                    nav_item('Vendor Kebersihan', 'broom', $halaman, 'vendor_kebersihan', 'vendor_kebersihan/');
                    nav_item('ATM', 'book', $halaman, 'atm', 'atm/');

                ?>

                <li class="nav-header">MAPPING</li>

                <?php

                    nav_item('Mapping ATM', 'map-marked-alt', $halaman, 'mapping', 'mapping/');

                ?>

                <li class="nav-header">ACTIVITY</li>

                <?php

                    nav_treeview('Installasi', 'book-reader', $halaman, [

                        [
                            'kd' => 'New',
                            'url' => 'installasi/new',
                            'halaman' => 'installasi_new',
                        ],
                        [
                            'kd' => 'Proses',
                            'url' => 'installasi/proses',
                            'halaman' => 'installasi_proses',
                        ],
                        [
                            'kd' => 'Finish',
                            'url' => 'installasi/finish',
                            'halaman' => 'installasi_finish',
                        ],

                    ]);

                    nav_treeview('Relokasi', 'book-reader', $halaman, [

                        [
                            'kd' => 'New',
                            'url' => 'relokasi/new',
                            'halaman' => 'relokasi_new',
                        ],
                        [
                            'kd' => 'Proses',
                            'url' => 'relokasi/proses',
                            'halaman' => 'relokasi_proses',
                        ],
                        [
                            'kd' => 'Finish',
                            'url' => 'relokasi/finish',
                            'halaman' => 'relokasi_finish',
                        ],

                    ]);

                    nav_treeview('Replacement', 'book-reader', $halaman, [

                        [
                            'kd' => 'New',
                            'url' => 'replacement/new',
                            'halaman' => 'replacement_new',
                        ],
                        [
                            'kd' => 'Proses',
                            'url' => 'replacement/proses',
                            'halaman' => 'replacement_proses',
                        ],
                        [
                            'kd' => 'Finish',
                            'url' => 'replacement/finish',
                            'halaman' => 'replacement_finish',
                        ],

                    ]);

                ?>

                <li class="nav-header">PROBLEM</li>

                <?php

                    nav_treeview('ATM Problem', 'book-reader', $halaman, [

                        [
                            'kd' => 'New',
                            'url' => 'atm_problem/new',
                            'halaman' => 'atm_problem_new',
                        ],
                        [
                            'kd' => 'Proses',
                            'url' => 'atm_problem/proses',
                            'halaman' => 'atm_problem_proses',
                        ],
                        [
                            'kd' => 'Finish',
                            'url' => 'atm_problem/finish',
                            'halaman' => 'atm_problem_finish',
                        ],

                    ]);

                    nav_treeview('Room Problem', 'book-reader', $halaman, [

                        [
                            'kd' => 'New',
                            'url' => 'room_problem/new',
                            'halaman' => 'room_problem_new',
                        ],
                        [
                            'kd' => 'Proses',
                            'url' => 'room_problem/proses',
                            'halaman' => 'room_problem_proses',
                        ],
                        [
                            'kd' => 'Finish',
                            'url' => 'room_problem/finish',
                            'halaman' => 'room_problem_finish',
                        ],

                    ]);

                ?>

                <li class="nav-header">ATM CHECKLIST</li>

                <?php

                    nav_item('Pengelola', 'edit', $halaman, 'checklist_pengelola', 'checklist/pengelola/');
                    nav_item('Kebersihan', 'edit', $halaman, 'checklist_kebersihan', 'checklist/kebersihan/');
                    nav_item('Checker', 'edit', $halaman, 'checklist_checker', 'checklist/checker/');

                ?>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>