<?php
$bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
?>
<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://ptmdm.co.id" target="_blank">PT. Mulia Daya Mandiri</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url('assets/') ?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/') ?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets/') ?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?= base_url('assets/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?= base_url('assets/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url('assets/') ?>plugins/moment/moment.min.js"></script>
<script src="<?= base_url('assets/') ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url('assets/') ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url('assets/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/') ?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url('assets/') ?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/') ?>dist/js/demo.js"></script>
<!-- pace-progress -->
<script src="<?= base_url('assets/') ?>plugins/pace-progress/pace.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?= base_url('assets/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/') ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
$(function() {
    $("#example1").DataTable();
    $("#example2").DataTable();
    $("#example3").DataTable();
    $("#example4").DataTable();
    $("#example5").DataTable();
    $("#example6").DataTable();
    $("#example7").DataTable();
    // $('#example2').DataTable({
    //     "paging": true,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false,
    // });
});
</script>
<?php if ($this->session->flashdata('success')) { ?>

<script>
Swal.fire(
    'Berhasil!',
    '<?= $this->session->flashdata('success') ?>',
    'success'
)
</script>

<?php } elseif ($this->session->flashdata('error')) { ?>

<script>
Swal.fire(
    'Gagal!',
    '<?= $this->session->flashdata('error') ?>',
    'error'
)
</script>

<?php } ?>
<script>
//--------------
//- AREA CHART -
//--------------

// Get context with jQuery - using jQuery's .get() method.
// var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

<?php if (isset($pref_cabang)) : ?>

<?php foreach ($pref_cabang as $cab) : ?>

$(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- BAR CHART -
    //-------------

    var chartData = {
        labels: [
            <?php for ($i=0; $i < count($bulan); $i++) { ?>

            '<?= $bulan[$i]; ?>',

            <?php } ?>
        ],
        datasets: [{
                label: '',
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,0,0,0)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0)',
                data: [
                    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ]
            },
            <?php if (isset($pref_atm_usage_2021)) : ?> {
                label: '2021',
                backgroundColor: 'rgba(138,43,226,0)',
                borderColor: 'rgba(138,43,226,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(138,43,226,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(138,43,226,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                            if ($pref_atm_usage_2021[$i]) { ?> '<?= $pref_atm_usage_2021[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                            endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($pref_atm_usage_2020)) : ?> {
                label: '2020',
                backgroundColor: 'rgba(255,0,0,0)',
                borderColor: 'rgba(255,0,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(255,0,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(255,0,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                            if ($pref_atm_usage_2020[$i]) { ?> '<?= $pref_atm_usage_2020[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                            endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($pref_atm_usage_2019)) : ?> {
                label: '2019',
                backgroundColor: 'rgba(0,255,0,0)',
                borderColor: 'rgba(0,255,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,255,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,255,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                            if ($pref_atm_usage_2019[$i]) { ?> '<?= $pref_atm_usage_2019[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                            endfor; ?>
                ]
            },
            <?php endif; ?>
        ]
    }

    var barChartCanvas = $('#perf_usage_tahunan').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, chartData)
    var temp0 = chartData.datasets[0]
    var temp1 = chartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'line',
        data: barChartData,
        options: barChartOptions
    })

})

<?php endforeach; ?>

<?php endif; ?>

<?php if (isset($dash_atm_usage)) : ?>

$(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- BAR CHART -
    //-------------

    var chartData = {
        labels: [
            <?php for ($i=0; $i < count($bulan); $i++) { ?>

            '<?= $bulan[$i]; ?>',

            <?php } ?>
        ],
        datasets: [{
                label: '',
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,0,0,0)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0)',
                data: [
                    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ]
            },
            <?php if (isset($dash_atm_usage_2021)) : ?> {
                label: '2021',
                backgroundColor: 'rgba(138,43,226,0)',
                borderColor: 'rgba(138,43,226,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(138,43,226,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(138,43,226,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_usage_2021[$i]) { ?> '<?= $dash_atm_usage_2021[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($dash_atm_usage_2020)) : ?> {
                label: '2020',
                backgroundColor: 'rgba(255,0,0,0)',
                borderColor: 'rgba(255,0,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(255,0,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(255,0,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_usage_2020[$i]) { ?> '<?= $dash_atm_usage_2020[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($dash_atm_usage_2019)) : ?> {
                label: '2019',
                backgroundColor: 'rgba(0,255,0,0)',
                borderColor: 'rgba(0,255,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,255,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,255,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_usage_2019[$i]) { ?> '<?= $dash_atm_usage_2019[$i][0]['value_atm_usage'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
        ]
    }

    var barChartCanvas = $('#usage_tahunan').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, chartData)
    var temp0 = chartData.datasets[0]
    var temp1 = chartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'line',
        data: barChartData,
        options: barChartOptions
    })

})

<?php endif; ?>

<?php if (isset($dash_atm_fbi)) : ?>

$(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- BAR CHART -
    //-------------

    var chartData = {
        labels: [
            <?php for ($i=0; $i < count($bulan); $i++) { ?>

            '<?= $bulan[$i]; ?>',

            <?php } ?>
        ],
        datasets: [{
                label: '',
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,0,0,0)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0)',
                data: [
                    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ]
            },
            <?php if (isset($dash_atm_fbi_2021)) : ?> {
                label: '2021',
                backgroundColor: 'rgba(138,43,226,0)',
                borderColor: 'rgba(138,43,226,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(138,43,226,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(138,43,226,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_fbi_2021[$i]) { ?> '<?= $dash_atm_fbi_2021[$i][0]['value_atm_fbi'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($dash_atm_fbi_2020)) : ?> {
                label: '2020',
                backgroundColor: 'rgba(255,0,0,0)',
                borderColor: 'rgba(255,0,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(255,0,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(255,0,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_fbi_2020[$i]) { ?> '<?= $dash_atm_fbi_2020[$i][0]['value_atm_fbi'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($dash_atm_fbi_2019)) : ?> {
                label: '2019',
                backgroundColor: 'rgba(0,255,0,0)',
                borderColor: 'rgba(0,255,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,255,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,255,0,1)',
                data: [
                    <?php for ($i = 1; $i <= 12; $i++) : 
                        if ($dash_atm_fbi_2019[$i]) { ?> '<?= $dash_atm_fbi_2019[$i][0]['value_atm_fbi'] ?>',
                    <?php } else { ?> '0',
                    <?php }
                        endfor; ?>
                ]
            },
            <?php endif; ?>
        ]
    }

    var barChartCanvas = $('#fbi_tahunan').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, chartData)
    var temp0 = chartData.datasets[0]
    var temp1 = chartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'line',
        data: barChartData,
        options: barChartOptions
    })

})

<?php endif; ?>

<?php if (isset($atm_usage)) : ?>

$(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- BAR CHART -
    //-------------

    var chartData = {
        labels: [
            <?php for ($i=0; $i < count($bulan); $i++) { ?>

            '<?= $bulan[$i]; ?>',

            <?php } ?>
        ],
        datasets: [{
                label: '',
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,0,0,0)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0)',
                data: [
                    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ]
            },
            <?php if (isset($atm_usage_2021)) : ?> {
                label: '2021',
                backgroundColor: 'rgba(138,43,226,0)',
                borderColor: 'rgba(138,43,226,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(138,43,226,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(138,43,226,1)',
                data: [
                    <?php foreach($atm_usage_2021 as $usg) : ?> '<?= $usg['value_atm_usage'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($atm_usage_2020)) : ?> {
                label: '2020',
                backgroundColor: 'rgba(255,0,0,0)',
                borderColor: 'rgba(255,0,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(255,0,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(255,0,0,1)',
                data: [
                    <?php foreach($atm_usage_2020 as $usg) : ?> '<?= $usg['value_atm_usage'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($atm_usage_2019)) : ?> {
                label: '2019',
                backgroundColor: 'rgba(0,255,0,0)',
                borderColor: 'rgba(0,255,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,255,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,255,0,1)',
                data: [
                    <?php foreach($atm_usage_2019 as $usg) : ?> '<?= $usg['value_atm_usage'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
        ]
    }

    var barChartCanvas = $('#usage').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, chartData)
    var temp0 = chartData.datasets[0]
    var temp1 = chartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'line',
        data: barChartData,
        options: barChartOptions
    })

})

<?php endif; ?>

<?php if (isset($atm_fbi)) : ?>

$(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- BAR CHART -
    //-------------

    var chartData = {
        labels: [
            <?php for ($i=0; $i < count($bulan); $i++) { ?>

            '<?= $bulan[$i]; ?>',

            <?php } ?>
        ],
        datasets: [{
                label: '',
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,0,0,0)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0)',
                data: [
                    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ]
            },
            <?php if (isset($atm_fbi_2021)) : ?> {
                label: '2021',
                backgroundColor: 'rgba(138,43,226,0)',
                borderColor: 'rgba(138,43,226,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(138,43,226,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(138,43,226,1)',
                data: [
                    <?php foreach($atm_fbi_2021 as $usg) : ?> '<?= $usg['value_atm_fbi'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($atm_fbi_2020)) : ?> {
                label: '2020',
                backgroundColor: 'rgba(255,0,0,0)',
                borderColor: 'rgba(255,0,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(255,0,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(255,0,0,1)',
                data: [
                    <?php foreach($atm_fbi_2020 as $usg) : ?> '<?= $usg['value_atm_fbi'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
            <?php if (isset($atm_fbi_2019)) : ?> {
                label: '2019',
                backgroundColor: 'rgba(0,255,0,0)',
                borderColor: 'rgba(0,255,0,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(0,255,0,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(0,255,0,1)',
                data: [
                    <?php foreach($atm_fbi_2019 as $usg) : ?> '<?= $usg['value_atm_fbi'] ?>',
                    <?php endforeach; ?>
                ]
            },
            <?php endif; ?>
        ]
    }

    var barChartCanvas = $('#fbi').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, chartData)
    var temp0 = chartData.datasets[0]
    var temp1 = chartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'line',
        data: barChartData,
        options: barChartOptions
    })

})

<?php endif; ?>
</script>
</body>

</html>