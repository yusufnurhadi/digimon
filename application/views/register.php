<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/logo/46.png">
  <title>Digimon | Daftar</title>
  <style type="text/css">
    body {
      background-size: cover;
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-image: url(img/pk3.jpg);
      transition: 2s linear;
      -moz-transition: 1.5s linear;
    }
  </style>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
</head>

<body class="hold-transition login-page pace-primary" style="background-size: cover;
                                                            background-repeat: no-repeat;
                                                            background-attachment: fixed;
                                                            background-image: url(<?= base_url('img/bg.jpg') ?>);
                                                            transition: 1.5s linear;
                                                            -moz-transition: 1.5s linear;
                                                            background-position: center;">
  <div class=" register-box">

  <div class="card" style="background-color:rgba(255,255,255,0.7); border-radius: 10px;">
    <div class="card-body register-card-body" style="background: transparent;">
    <img src="<?= base_url('img/logo/bni.png') ?>" style="max-width: 300px;"><br><br>
        <p class="login-box-msg">Digital Monitoring System ATR WJY</p>

      <form action="<?= base_url('register/') ?>" method="post">
        <div class="mt-3">
          <?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <input type="text" name="nama" class="form-control" placeholder="Full name" value="<?= set_value('nama') ?>" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <input type="email" name="email" class="form-control" placeholder="Email" value="<?= set_value('email') ?>" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('no_telp', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <input type="text" name="no_telp" class="form-control" placeholder="No Telpon" value="<?= set_value('no_telp') ?>" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-phone"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <input type="password" name="password1" class="form-control" placeholder="Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <input type="password" name="password2" class="form-control" placeholder="Ulangi password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('akses', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <select class="form-control" name="akses" required>
              <option value="">-- Pilih Hak Akses --</option>
              <option value="admin">Administrator</option>
              <option value="wilayah">Wilayah</option>
              <option value="cabang">Cabang</option>
              <option value="checker">Checker</option>
              <option value="pengelola">Pengelola</option>
              <option value="pengelola_teknisi">Teknisi Pengelola</option>
              <option value="kebersihan">Kebersihan</option>
              <option value="kebersihan_operator">Operator Kebersihan</option>
            </select>
          </div>
        </div>
        <div class="mt-3">
          <?= form_error('atm_cabang', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <select class="form-control" name="atm_cabang">
              <option value="">-- Pilih Cabang ATM --</option>
              <?php foreach ($atm_cabang as $key) : ?>
                <option value="<?= $key['kd_atm_cabang'] ?>"><?= $key['nama_atm_cabang'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <p class="text-xs">* Hanya untuk User Cabang dan Checker</p>
        </div>
        <div class="mt-3">
          <?= form_error('atm_pengelola', '<small class="text-danger">', '</small>'); ?>
          <div class="input-group">
            <select class="form-control" name="atm_pengelola">
              <option value="">-- Pilih Pengelola ATM --</option>
              <?php foreach ($atm_pengelola as $key) : ?>
                <option value="<?= $key['kd_atm_pengelola'] ?>"><?= $key['nama_atm_pengelola'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <p class="text-xs">* Hanya untuk User Pengelola dan Teknisi Pengelola</p>
        </div>
        <div class="row mt-3">
          <div class="col-8">
            <div class="icheck-primary">
              Sudah punya akun? <a href="<?= base_url() ?>" class="text-center">Masuk</a>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Daftar">
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
  </div>
  <!-- /.register-box -->
  <!-- jQuery -->
  <script src="<?= base_url('assets/') ?>plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url('assets/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('assets/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url('assets/') ?>plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="<?= base_url('assets/') ?>plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="<?= base_url('assets/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?= base_url('assets/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url('assets/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url('assets/') ?>plugins/moment/moment.min.js"></script>
  <script src="<?= base_url('assets/') ?>plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="<?= base_url('assets/') ?>plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url('assets/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url('assets/') ?>dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url('assets/') ?>dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url('assets/') ?>dist/js/demo.js"></script>
  <!-- pace-progress -->
  <script src="<?= base_url('assets/') ?>plugins/pace-progress/pace.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url('assets/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
  <?php if ($this->session->flashdata('success')) { ?>

    <script>
      Swal.fire(
        'Berhasil!',
        '<?= $this->session->flashdata('success') ?>',
        'success'
      )
    </script>

  <?php } elseif ($this->session->flashdata('error')) { ?>

    <script>
      Swal.fire(
        'Gagal!',
        '<?= $this->session->flashdata('error') ?>',
        'error'
      )
    </script>

  <?php } ?>
</body>

</html>