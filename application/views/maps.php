<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    #map {
        height: 100%;
    }

    /* Optional: Makes the sample page fill the window. */
    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    </style>
</head>

<body>
    <div id="map"></div>
    <script>
    function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: {
                lat: -6.232224,
                lng: 106.922370
            }
        });

        <?php foreach ($atm as $key) : ?>

        var marker = new google.maps.Marker({
            position: {
                lat: <?= $key['lat_atm'] ?>,
                lng: <?= $key['lng_atm'] ?>
            },
            map: map,
            title: '<?= $key['lokasi_atm'] ?>'
        });

        <?php endforeach; ?>

    }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5noTq0x877l7aX1PrMsXj9F0F04KXW9Y&callback=initMap">
    </script>
</body>

</html>