<?php

if ($this->session->userdata('email_user')) {

  if ($this->session->userdata('role_user')=="admin") {

    include 'admin/templates/header.php';
    include 'admin/templates/sidebar.php';
    if (isset($halaman)) {
      include 'admin/content/' . $halaman . '.php';
    } else {
      include 'admin/content/dashboard.php';
    }
    include 'admin/templates/footer.php';
  
  } else {

    $this->session->set_flashdata('error', 'Anda harus login dahulu ');
    redirect();
    die();
  
  }

} else {

  $this->session->set_flashdata('error', 'Anda harus login dahulu ');
  redirect();
  die();

}
