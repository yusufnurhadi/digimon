-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2020 at 09:49 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digimon`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_installasi`
--

CREATE TABLE `activity_installasi` (
  `id_activity_installasi` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `lokasi_activity_installasi` text NOT NULL,
  `tanggal_activity_installasi` date NOT NULL,
  `vendor_activity_installasi` varchar(100) NOT NULL,
  `status_activity_installasi` enum('new','proses','finish') NOT NULL DEFAULT 'new',
  `created_activity_installasi` datetime NOT NULL,
  `updated_activity_installasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity_relokasi`
--

CREATE TABLE `activity_relokasi` (
  `id_activity_relokasi` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `lokasi_lama_activity_relokasi` text NOT NULL,
  `lokasi_baru_activity_relokasi` text NOT NULL,
  `tanggal_activity_relokasi` date NOT NULL,
  `vendor_activity_relokasi` varchar(100) NOT NULL,
  `status_activity_relokasi` enum('new','proses','finish') NOT NULL DEFAULT 'new',
  `created_activity_relokasi` datetime NOT NULL,
  `updated_activity_relokasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity_replacement`
--

CREATE TABLE `activity_replacement` (
  `id_activity_replacement` int(11) NOT NULL,
  `id_atm_lama_activity_replacement` varchar(100) NOT NULL,
  `id_atm_baru_activity_replacement` varchar(100) NOT NULL,
  `lokasi_activity_replacement` int(11) NOT NULL,
  `tanggal_activity_replacement` date NOT NULL,
  `vendor_activity_replacement` varchar(100) NOT NULL,
  `status_activity_replacement` enum('new','proses','finish') NOT NULL DEFAULT 'new',
  `created_activity_replacement` datetime NOT NULL,
  `updated_activity_replacement` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `created_admin` datetime NOT NULL,
  `updated_admin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm`
--

CREATE TABLE `atm` (
  `id_atm` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `lokasi_atm` text NOT NULL,
  `alamat_atm` longtext NOT NULL,
  `kelurahan_atm` text NOT NULL,
  `kecamatan_atm` text NOT NULL,
  `kota_atm` text NOT NULL,
  `provinsi_atm` text NOT NULL,
  `lat_atm` float(10,6) NOT NULL,
  `lng_atm` float(10,6) NOT NULL,
  `status_atm` enum('on','off') NOT NULL DEFAULT 'on',
  `jenis_atm` varchar(100) NOT NULL,
  `merk_mesin_atm` varchar(100) NOT NULL,
  `jenis_mesin_atm` varchar(100) NOT NULL,
  `sn_mesin_atm` varchar(100) NOT NULL,
  `denom_atm` varchar(100) NOT NULL,
  `jarkom_atm` varchar(100) NOT NULL,
  `merk_ups_atm` varchar(100) NOT NULL,
  `sn_ups_atm` varchar(100) NOT NULL,
  `merk_dome_atm` varchar(100) NOT NULL,
  `sn_dome_atm` varchar(100) NOT NULL,
  `merk_dvr_atm` varchar(100) NOT NULL,
  `sn_dvr_atm` varchar(100) NOT NULL,
  `created_atm` datetime NOT NULL,
  `updated_atm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_cabang`
--

CREATE TABLE `atm_cabang` (
  `kd_atm_cabang` varchar(100) NOT NULL,
  `nama_atm_cabang` varchar(100) NOT NULL,
  `created_atm_cabang` datetime NOT NULL,
  `updated_atm_cabang` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_fbi`
--

CREATE TABLE `atm_fbi` (
  `id_atm_fbi` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `value_atm_fbi` int(11) NOT NULL,
  `month_atm_fbi` int(11) NOT NULL,
  `year_atm_fbi` int(11) NOT NULL,
  `created_atm_fbi` datetime NOT NULL,
  `updated_atm_fbi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_kebersihan`
--

CREATE TABLE `atm_kebersihan` (
  `id_atm_kebersihan` int(11) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `nama_atm_kebersihan` varchar(100) NOT NULL,
  `created_atm_kebersihan` datetime NOT NULL,
  `updated_atm_kebersihan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_pengelola`
--

CREATE TABLE `atm_pengelola` (
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `nama_atm_pengelola` varchar(100) NOT NULL,
  `created_atm_pengelola` datetime NOT NULL,
  `updated_atm_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_problem`
--

CREATE TABLE `atm_problem` (
  `id_atm_problem` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `no_atm_problem` varchar(100) NOT NULL,
  `pelapor_atm_problem` varchar(100) NOT NULL,
  `status_pelapor_atm_problem` enum('admin','cabang','wilayah','pengelola','checker','kebersihan','pengelola_teknisi','kebersihan_operator') NOT NULL,
  `problem_atm_problem` text NOT NULL,
  `foto_atm_problem` varchar(100) NOT NULL,
  `status_atm_problem` enum('new','proses','finish') NOT NULL DEFAULT 'new',
  `created_atm_problem` datetime NOT NULL,
  `update_atm_problem` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_problem_detail`
--

CREATE TABLE `atm_problem_detail` (
  `id_atm_problem_detail` int(11) NOT NULL,
  `id_atm_problem` int(11) NOT NULL,
  `id_pengelola` int(11) NOT NULL,
  `id_pengelola_teknisi` int(11) NOT NULL,
  `catatan_atm_problem_detail` text NOT NULL,
  `created_atm_problem_detail` datetime NOT NULL,
  `updated_atm_problem_detail` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_problem_finish`
--

CREATE TABLE `atm_problem_finish` (
  `id_atm_problem_finish` int(11) NOT NULL,
  `id_atm_problem` int(11) NOT NULL,
  `id_pengelola_teknisi` int(11) NOT NULL,
  `foto_atm_problem_finish` varchar(100) NOT NULL,
  `catatan_atm_problem_finish` text NOT NULL,
  `created_atm_problem_finish` datetime NOT NULL,
  `updated_atm_problem_finish` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_sla`
--

CREATE TABLE `atm_sla` (
  `id_atm_sla` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `value_atm_sla` int(11) NOT NULL,
  `month_atm_sla` int(11) NOT NULL,
  `year_atm_sla` int(11) NOT NULL,
  `created_atm_sla` datetime NOT NULL,
  `updated_atm_sla` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atm_usage`
--

CREATE TABLE `atm_usage` (
  `id_atm_usage` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `value_atm_usage` int(11) NOT NULL,
  `month_atm_usage` int(11) NOT NULL,
  `year_atm_usage` int(11) NOT NULL,
  `created_atm_usage` datetime NOT NULL,
  `updated_atm_usage` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id_cabang` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `nama_cabang` varchar(100) NOT NULL,
  `created_cabang` datetime NOT NULL,
  `updated_cabang` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checker`
--

CREATE TABLE `checker` (
  `id_checker` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `kd_atm_cabang` varchar(100) NOT NULL,
  `nama_checker` varchar(100) NOT NULL,
  `created_checker` datetime NOT NULL,
  `updated_checker` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_checker`
--

CREATE TABLE `checklist_checker` (
  `id_checklist_checker` int(11) NOT NULL,
  `id_checker` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `ac_checklist_checker` enum('sesuai','tidak sesuai') NOT NULL,
  `stiker_checklist_checker` enum('sesuai','tidak sesuai') NOT NULL,
  `polesign_checklist_checker` enum('sesuai','tidak sesuai') NOT NULL,
  `booth_checklist_checker` enum('sesuai','tidak sesuai') NOT NULL,
  `token_checklist_checker` int(11) NOT NULL,
  `foto_lokasi_checklist_checker` varchar(100) NOT NULL,
  `created_checklist_checker` datetime NOT NULL,
  `updated_checklist_checker` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_kebersihan`
--

CREATE TABLE `checklist_kebersihan` (
  `id_checklist_kebersihan` int(11) NOT NULL,
  `id_kebersihan_opr` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `ac_checklist_kebersihan` enum('sesuai','tidak sesuai') NOT NULL,
  `stiker_checklist_kebersihan` enum('sesuai','tidak sesuai') NOT NULL,
  `polesign_checklist_kebersihan` enum('sesuai','tidak sesuai') NOT NULL,
  `booth_checklist_kebersihan` enum('sesuai','tidak sesuai') NOT NULL,
  `foto_lokasi_checklist_kebersihan` varchar(100) NOT NULL,
  `created_checklist_kebersihan` datetime NOT NULL,
  `updated_checklist_kebersihan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_pengelola`
--

CREATE TABLE `checklist_pengelola` (
  `id_checklist_pengelola` int(11) NOT NULL,
  `id_pengelola_teknisi` int(11) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `ups_checklist_pengelola` enum('sesuai','tidak sesuai') NOT NULL,
  `dvr_checklist_pengelola` enum('sesuai','tidak sesuai') NOT NULL,
  `jenis_mesin_checklist_pengelola` varchar(100) NOT NULL,
  `sn_mesin_checklist_pengelola` varchar(100) NOT NULL,
  `denom_checklist_pengelola` varchar(100) NOT NULL,
  `grounding_checklist_pengelola` varchar(100) NOT NULL,
  `gembok_booth_checklist_pengelola` enum('sesuai','tidak sesuai') NOT NULL,
  `foto_id_checklist_pengelola` varchar(100) NOT NULL,
  `foto_lokasi_checklist_pengelola` varchar(100) NOT NULL,
  `foto_sn_mesin_checklist_pengelola` varchar(100) NOT NULL,
  `created_checklist_pengelola` datetime NOT NULL,
  `updated_checklist_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kebersihan`
--

CREATE TABLE `kebersihan` (
  `id_kebersihan` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `id_atm_kebersihan` int(11) NOT NULL,
  `nama_kebersihan` varchar(100) NOT NULL,
  `created_kebersihan` datetime NOT NULL,
  `updated_kebersihan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kebersihan_opr`
--

CREATE TABLE `kebersihan_opr` (
  `id_kebersihan_opr` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `id_atm_kebersihan` int(11) NOT NULL,
  `nama_kebersihan_opr` varchar(100) NOT NULL,
  `created_kebersihan_opr` datetime NOT NULL,
  `updated_kebersihan_opr` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengelola`
--

CREATE TABLE `pengelola` (
  `id_pengelola` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `nama_pengelola` varchar(100) NOT NULL,
  `created_pengelola` datetime NOT NULL,
  `updated_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengelola_teknisi`
--

CREATE TABLE `pengelola_teknisi` (
  `id_pengelola_teknisi` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `kd_atm_pengelola` varchar(100) NOT NULL,
  `nama_pengelola_teknisi` varchar(100) NOT NULL,
  `created_pengelola_teknisi` datetime NOT NULL,
  `updated_pengelola_teknisi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_problem`
--

CREATE TABLE `room_problem` (
  `id_room_problem` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `id_atm` varchar(100) NOT NULL,
  `no_room_problem` varchar(100) NOT NULL,
  `pelapor_room_problem` varchar(100) NOT NULL,
  `status_pelapor_room_problem` enum('admin','cabang','wilayah','pengelola','checker','kebersihan','pengelola_teknisi','kebersihan_opr') NOT NULL,
  `problem_room_problem` text NOT NULL,
  `foto_room_problem` varchar(100) NOT NULL,
  `status_room_problem` enum('new','proses','finish') NOT NULL DEFAULT 'new',
  `created_room_problem` datetime NOT NULL,
  `updated_room_problem` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_problem_detail`
--

CREATE TABLE `room_problem_detail` (
  `id_room_problem_detail` int(11) NOT NULL,
  `id_room_problem` int(11) NOT NULL,
  `id_kebersihan` int(11) NOT NULL,
  `id_kebersihan_opr` int(11) NOT NULL,
  `catatan_room_problem_detail` text NOT NULL,
  `created_room_problem_detail` datetime NOT NULL,
  `updated_room_problem_detail` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_problem_finish`
--

CREATE TABLE `room_problem_finish` (
  `id_room_problem_finish` int(11) NOT NULL,
  `id_room_problem` int(11) NOT NULL,
  `id_kebersihan_opr` int(11) NOT NULL,
  `foto_room_problem_finish` varchar(100) NOT NULL,
  `catatan_room_problem_finish` text NOT NULL,
  `created_room_problem_finish` datetime NOT NULL,
  `updated_room_problem_finish` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `token_checker`
--

CREATE TABLE `token_checker` (
  `id_token_checker` varchar(190) NOT NULL,
  `id_checker` int(11) NOT NULL,
  `created_token_checker` datetime NOT NULL,
  `updated_token_checker` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `token_kebersihan_opr`
--

CREATE TABLE `token_kebersihan_opr` (
  `id_token_kebersihan_opr` varchar(190) NOT NULL,
  `id_kebersihan_opr` int(11) NOT NULL,
  `created_token_kebersihan_opr` datetime NOT NULL,
  `updated_token_kebersihan_opr` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `token_pengelola_teknisi`
--

CREATE TABLE `token_pengelola_teknisi` (
  `id_token_pengelola_teknisi` varchar(190) NOT NULL,
  `id_pengelola_teknisi` int(11) NOT NULL,
  `created_token_pengelola_teknisi` datetime NOT NULL,
  `updated_token_pengelola_teknisi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `email_user` varchar(100) NOT NULL,
  `password_user` text NOT NULL,
  `no_user` varchar(100) NOT NULL,
  `foto_user` varchar(100) NOT NULL DEFAULT 'default.jpg',
  `role_user` enum('admin','cabang','wilayah','pengelola','checker','kebersihan','pengelola_teknisi','kebersihan_operator') NOT NULL,
  `is_active_user` tinyint(4) NOT NULL DEFAULT '0',
  `notif_user` tinyint(4) NOT NULL DEFAULT '1',
  `notif_email_user` tinyint(4) NOT NULL DEFAULT '1',
  `created_user` datetime NOT NULL,
  `updated_user` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_installasi`
--
ALTER TABLE `activity_installasi`
  ADD PRIMARY KEY (`id_activity_installasi`),
  ADD KEY `fk_activity_installasi_1` (`id_atm`);

--
-- Indexes for table `activity_relokasi`
--
ALTER TABLE `activity_relokasi`
  ADD PRIMARY KEY (`id_activity_relokasi`),
  ADD KEY `fk_activity_relokasi_1` (`id_atm`);

--
-- Indexes for table `activity_replacement`
--
ALTER TABLE `activity_replacement`
  ADD PRIMARY KEY (`id_activity_replacement`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `fk_admin_1` (`email_user`);

--
-- Indexes for table `atm`
--
ALTER TABLE `atm`
  ADD PRIMARY KEY (`id_atm`),
  ADD KEY `fk_atm_1` (`kd_atm_cabang`),
  ADD KEY `fk_atm_2` (`kd_atm_pengelola`);

--
-- Indexes for table `atm_cabang`
--
ALTER TABLE `atm_cabang`
  ADD PRIMARY KEY (`kd_atm_cabang`);

--
-- Indexes for table `atm_fbi`
--
ALTER TABLE `atm_fbi`
  ADD PRIMARY KEY (`id_atm_fbi`),
  ADD KEY `fk_atm_fbi_1` (`id_atm`),
  ADD KEY `fk_atm_fbi_2` (`kd_atm_cabang`),
  ADD KEY `fk_atm_fbi_3` (`kd_atm_pengelola`);

--
-- Indexes for table `atm_kebersihan`
--
ALTER TABLE `atm_kebersihan`
  ADD PRIMARY KEY (`id_atm_kebersihan`),
  ADD KEY `fk_atm_kebersihan_1` (`kd_atm_cabang`);

--
-- Indexes for table `atm_pengelola`
--
ALTER TABLE `atm_pengelola`
  ADD PRIMARY KEY (`kd_atm_pengelola`);

--
-- Indexes for table `atm_problem`
--
ALTER TABLE `atm_problem`
  ADD PRIMARY KEY (`id_atm_problem`),
  ADD KEY `fk_atm_problem_1` (`email_user`),
  ADD KEY `fk_atm_problem_2` (`id_atm`);

--
-- Indexes for table `atm_problem_detail`
--
ALTER TABLE `atm_problem_detail`
  ADD PRIMARY KEY (`id_atm_problem_detail`),
  ADD KEY `fk_atm_problem_detail_1` (`id_atm_problem`),
  ADD KEY `fk_atm_problem_detail_2` (`id_pengelola`),
  ADD KEY `fk_atm_problem_detail_3` (`id_pengelola_teknisi`);

--
-- Indexes for table `atm_problem_finish`
--
ALTER TABLE `atm_problem_finish`
  ADD PRIMARY KEY (`id_atm_problem_finish`),
  ADD KEY `fk_atm_problem_finish_1` (`id_atm_problem`),
  ADD KEY `fk_atm_problem_finish_2` (`id_pengelola_teknisi`);

--
-- Indexes for table `atm_sla`
--
ALTER TABLE `atm_sla`
  ADD PRIMARY KEY (`id_atm_sla`),
  ADD KEY `fk_atm_sla_1` (`id_atm`),
  ADD KEY `fk_atm_sla_2` (`kd_atm_cabang`),
  ADD KEY `fk_atm_sla_3` (`kd_atm_pengelola`);

--
-- Indexes for table `atm_usage`
--
ALTER TABLE `atm_usage`
  ADD PRIMARY KEY (`id_atm_usage`),
  ADD KEY `fk_atm_usage_1` (`id_atm`),
  ADD KEY `fk_atm_usage_2` (`kd_atm_cabang`),
  ADD KEY `fk_atm_usage_3` (`kd_atm_pengelola`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id_cabang`),
  ADD KEY `fk_cabang_1` (`email_user`),
  ADD KEY `fk_cabang_2` (`kd_atm_cabang`);

--
-- Indexes for table `checker`
--
ALTER TABLE `checker`
  ADD PRIMARY KEY (`id_checker`),
  ADD KEY `fk_checker_1` (`email_user`),
  ADD KEY `fk_checker_2` (`kd_atm_cabang`);

--
-- Indexes for table `checklist_checker`
--
ALTER TABLE `checklist_checker`
  ADD PRIMARY KEY (`id_checklist_checker`),
  ADD KEY `fk_checklist_checker_1` (`id_checker`),
  ADD KEY `fk_checklist_checker_2` (`id_atm`);

--
-- Indexes for table `checklist_kebersihan`
--
ALTER TABLE `checklist_kebersihan`
  ADD PRIMARY KEY (`id_checklist_kebersihan`),
  ADD KEY `fk_checklist_kebersihan_1` (`id_kebersihan_opr`),
  ADD KEY `fk_checklist_kebersihan_2` (`id_atm`);

--
-- Indexes for table `checklist_pengelola`
--
ALTER TABLE `checklist_pengelola`
  ADD PRIMARY KEY (`id_checklist_pengelola`),
  ADD KEY `fk_checklist_pengelola_1` (`id_pengelola_teknisi`),
  ADD KEY `fk_checklist_pengelola_2` (`id_atm`);

--
-- Indexes for table `kebersihan`
--
ALTER TABLE `kebersihan`
  ADD PRIMARY KEY (`id_kebersihan`),
  ADD KEY `fk_kebersihan_1` (`email_user`),
  ADD KEY `fk_kebersihan_2` (`id_atm_kebersihan`);

--
-- Indexes for table `kebersihan_opr`
--
ALTER TABLE `kebersihan_opr`
  ADD PRIMARY KEY (`id_kebersihan_opr`),
  ADD KEY `fk_kebersihan_opr_1` (`email_user`),
  ADD KEY `fk_kebersihan_opr_2` (`id_atm_kebersihan`);

--
-- Indexes for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD PRIMARY KEY (`id_pengelola`),
  ADD KEY `fk_pengelola_1` (`email_user`),
  ADD KEY `fk_pengelola_2` (`kd_atm_pengelola`);

--
-- Indexes for table `pengelola_teknisi`
--
ALTER TABLE `pengelola_teknisi`
  ADD PRIMARY KEY (`id_pengelola_teknisi`),
  ADD KEY `fk_pengelola_teknisi_1` (`email_user`),
  ADD KEY `fk_pengelola_teknisi_2` (`kd_atm_pengelola`);

--
-- Indexes for table `room_problem`
--
ALTER TABLE `room_problem`
  ADD PRIMARY KEY (`id_room_problem`),
  ADD KEY `fk_room_problem_1` (`email_user`),
  ADD KEY `fk_room_problem_2` (`id_atm`);

--
-- Indexes for table `room_problem_detail`
--
ALTER TABLE `room_problem_detail`
  ADD PRIMARY KEY (`id_room_problem_detail`),
  ADD KEY `room_problem_detail_1` (`id_room_problem`),
  ADD KEY `room_problem_detail_2` (`id_kebersihan`),
  ADD KEY `room_problem_detail_3` (`id_kebersihan_opr`);

--
-- Indexes for table `room_problem_finish`
--
ALTER TABLE `room_problem_finish`
  ADD PRIMARY KEY (`id_room_problem_finish`),
  ADD KEY `fk_room_problem_finish_1` (`id_room_problem`),
  ADD KEY `fk_room_problem_finish_2` (`id_kebersihan_opr`);

--
-- Indexes for table `token_checker`
--
ALTER TABLE `token_checker`
  ADD PRIMARY KEY (`id_token_checker`),
  ADD KEY `fk_token_checker_1` (`id_checker`);

--
-- Indexes for table `token_kebersihan_opr`
--
ALTER TABLE `token_kebersihan_opr`
  ADD PRIMARY KEY (`id_token_kebersihan_opr`),
  ADD KEY `fk_token_kebersihan_opr` (`id_kebersihan_opr`);

--
-- Indexes for table `token_pengelola_teknisi`
--
ALTER TABLE `token_pengelola_teknisi`
  ADD PRIMARY KEY (`id_token_pengelola_teknisi`),
  ADD KEY `fk_token_pengelola_teknisi_1` (`id_pengelola_teknisi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_installasi`
--
ALTER TABLE `activity_installasi`
  MODIFY `id_activity_installasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `activity_relokasi`
--
ALTER TABLE `activity_relokasi`
  MODIFY `id_activity_relokasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `activity_replacement`
--
ALTER TABLE `activity_replacement`
  MODIFY `id_activity_replacement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atm_kebersihan`
--
ALTER TABLE `atm_kebersihan`
  MODIFY `id_atm_kebersihan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atm_problem`
--
ALTER TABLE `atm_problem`
  MODIFY `id_atm_problem` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atm_problem_detail`
--
ALTER TABLE `atm_problem_detail`
  MODIFY `id_atm_problem_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atm_problem_finish`
--
ALTER TABLE `atm_problem_finish`
  MODIFY `id_atm_problem_finish` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id_cabang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checker`
--
ALTER TABLE `checker`
  MODIFY `id_checker` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checklist_checker`
--
ALTER TABLE `checklist_checker`
  MODIFY `id_checklist_checker` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checklist_kebersihan`
--
ALTER TABLE `checklist_kebersihan`
  MODIFY `id_checklist_kebersihan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checklist_pengelola`
--
ALTER TABLE `checklist_pengelola`
  MODIFY `id_checklist_pengelola` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kebersihan`
--
ALTER TABLE `kebersihan`
  MODIFY `id_kebersihan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kebersihan_opr`
--
ALTER TABLE `kebersihan_opr`
  MODIFY `id_kebersihan_opr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengelola`
--
ALTER TABLE `pengelola`
  MODIFY `id_pengelola` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengelola_teknisi`
--
ALTER TABLE `pengelola_teknisi`
  MODIFY `id_pengelola_teknisi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room_problem`
--
ALTER TABLE `room_problem`
  MODIFY `id_room_problem` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room_problem_detail`
--
ALTER TABLE `room_problem_detail`
  MODIFY `id_room_problem_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room_problem_finish`
--
ALTER TABLE `room_problem_finish`
  MODIFY `id_room_problem_finish` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_installasi`
--
ALTER TABLE `activity_installasi`
  ADD CONSTRAINT `fk_activity_installasi_1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `activity_relokasi`
--
ALTER TABLE `activity_relokasi`
  ADD CONSTRAINT `fk_activity_relokasi_1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_admin_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`);

--
-- Constraints for table `atm`
--
ALTER TABLE `atm`
  ADD CONSTRAINT `fk_atm_1` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`),
  ADD CONSTRAINT `fk_atm_2` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `atm_fbi`
--
ALTER TABLE `atm_fbi`
  ADD CONSTRAINT `fk_atm_fbi_1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`),
  ADD CONSTRAINT `fk_atm_fbi_2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`),
  ADD CONSTRAINT `fk_atm_fbi_3` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `atm_kebersihan`
--
ALTER TABLE `atm_kebersihan`
  ADD CONSTRAINT `fk_atm_kebersihan_1` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`);

--
-- Constraints for table `atm_problem`
--
ALTER TABLE `atm_problem`
  ADD CONSTRAINT `fk_atm_problem_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_atm_problem_2` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `atm_problem_detail`
--
ALTER TABLE `atm_problem_detail`
  ADD CONSTRAINT `fk_atm_problem_detail_1` FOREIGN KEY (`id_atm_problem`) REFERENCES `atm_problem` (`id_atm_problem`),
  ADD CONSTRAINT `fk_atm_problem_detail_2` FOREIGN KEY (`id_pengelola`) REFERENCES `pengelola` (`id_pengelola`),
  ADD CONSTRAINT `fk_atm_problem_detail_3` FOREIGN KEY (`id_pengelola_teknisi`) REFERENCES `pengelola_teknisi` (`id_pengelola_teknisi`);

--
-- Constraints for table `atm_problem_finish`
--
ALTER TABLE `atm_problem_finish`
  ADD CONSTRAINT `fk_atm_problem_finish_1` FOREIGN KEY (`id_atm_problem`) REFERENCES `atm_problem` (`id_atm_problem`),
  ADD CONSTRAINT `fk_atm_problem_finish_2` FOREIGN KEY (`id_pengelola_teknisi`) REFERENCES `pengelola_teknisi` (`id_pengelola_teknisi`);

--
-- Constraints for table `atm_sla`
--
ALTER TABLE `atm_sla`
  ADD CONSTRAINT `fk_atm_sla_1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`),
  ADD CONSTRAINT `fk_atm_sla_2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`),
  ADD CONSTRAINT `fk_atm_sla_3` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `atm_usage`
--
ALTER TABLE `atm_usage`
  ADD CONSTRAINT `fk_atm_usage_1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`),
  ADD CONSTRAINT `fk_atm_usage_2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`),
  ADD CONSTRAINT `fk_atm_usage_3` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `cabang`
--
ALTER TABLE `cabang`
  ADD CONSTRAINT `fk_cabang_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_cabang_2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`);

--
-- Constraints for table `checker`
--
ALTER TABLE `checker`
  ADD CONSTRAINT `fk_checker_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_checker_2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`);

--
-- Constraints for table `checklist_checker`
--
ALTER TABLE `checklist_checker`
  ADD CONSTRAINT `fk_checklist_checker_1` FOREIGN KEY (`id_checker`) REFERENCES `checker` (`id_checker`),
  ADD CONSTRAINT `fk_checklist_checker_2` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `checklist_kebersihan`
--
ALTER TABLE `checklist_kebersihan`
  ADD CONSTRAINT `fk_checklist_kebersihan_1` FOREIGN KEY (`id_kebersihan_opr`) REFERENCES `kebersihan_opr` (`id_kebersihan_opr`),
  ADD CONSTRAINT `fk_checklist_kebersihan_2` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `checklist_pengelola`
--
ALTER TABLE `checklist_pengelola`
  ADD CONSTRAINT `fk_checklist_pengelola_1` FOREIGN KEY (`id_pengelola_teknisi`) REFERENCES `pengelola_teknisi` (`id_pengelola_teknisi`),
  ADD CONSTRAINT `fk_checklist_pengelola_2` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `kebersihan`
--
ALTER TABLE `kebersihan`
  ADD CONSTRAINT `fk_kebersihan_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_kebersihan_2` FOREIGN KEY (`id_atm_kebersihan`) REFERENCES `atm_kebersihan` (`id_atm_kebersihan`);

--
-- Constraints for table `kebersihan_opr`
--
ALTER TABLE `kebersihan_opr`
  ADD CONSTRAINT `fk_kebersihan_opr_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_kebersihan_opr_2` FOREIGN KEY (`id_atm_kebersihan`) REFERENCES `atm_kebersihan` (`id_atm_kebersihan`);

--
-- Constraints for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD CONSTRAINT `fk_pengelola_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_pengelola_2` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `pengelola_teknisi`
--
ALTER TABLE `pengelola_teknisi`
  ADD CONSTRAINT `fk_pengelola_teknisi_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_pengelola_teknisi_2` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `room_problem`
--
ALTER TABLE `room_problem`
  ADD CONSTRAINT `fk_room_problem_1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fk_room_problem_2` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`);

--
-- Constraints for table `room_problem_detail`
--
ALTER TABLE `room_problem_detail`
  ADD CONSTRAINT `room_problem_detail_1` FOREIGN KEY (`id_room_problem`) REFERENCES `room_problem` (`id_room_problem`),
  ADD CONSTRAINT `room_problem_detail_2` FOREIGN KEY (`id_kebersihan`) REFERENCES `kebersihan` (`id_kebersihan`),
  ADD CONSTRAINT `room_problem_detail_3` FOREIGN KEY (`id_kebersihan_opr`) REFERENCES `kebersihan_opr` (`id_kebersihan_opr`);

--
-- Constraints for table `room_problem_finish`
--
ALTER TABLE `room_problem_finish`
  ADD CONSTRAINT `fk_room_problem_finish_1` FOREIGN KEY (`id_room_problem`) REFERENCES `room_problem` (`id_room_problem`),
  ADD CONSTRAINT `fk_room_problem_finish_2` FOREIGN KEY (`id_kebersihan_opr`) REFERENCES `kebersihan_opr` (`id_kebersihan_opr`);

--
-- Constraints for table `token_checker`
--
ALTER TABLE `token_checker`
  ADD CONSTRAINT `fk_token_checker_1` FOREIGN KEY (`id_checker`) REFERENCES `checker` (`id_checker`);

--
-- Constraints for table `token_kebersihan_opr`
--
ALTER TABLE `token_kebersihan_opr`
  ADD CONSTRAINT `fk_token_kebersihan_opr` FOREIGN KEY (`id_kebersihan_opr`) REFERENCES `kebersihan_opr` (`id_kebersihan_opr`);

--
-- Constraints for table `token_pengelola_teknisi`
--
ALTER TABLE `token_pengelola_teknisi`
  ADD CONSTRAINT `fk_token_pengelola_teknisi_1` FOREIGN KEY (`id_pengelola_teknisi`) REFERENCES `pengelola_teknisi` (`id_pengelola_teknisi`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
